<?php
use App\Models\Product;
use App\Models\Review;
?>
@extends('layouts.ecommerce')
<!-- seo and social share content -->
@if($product)
  @section('title', $product->name)
@endif

@if($product)
    @section('social_title', $product->name)
    @section('social_description', $product->long_descriptions)
    @section('social_image',  env('APP_URL').'/'.$product->image1 )
@endif

@section('content')
        <!-- Begin Hiraola's Breadcrumb Area -->
        <!-- <div class="breadcrumb-area">
            <div class="container">
                <div class="breadcrumb-content">
                    <h2>{{strtoupper($product->name)}}</h2>
                    <ul>
                        <li><a href="/">Home</a></li>
                        <li class="active">{{strtoupper($product->name)}}</li>
                    </ul>
                </div>
            </div>
        </div> -->
        <!-- Hiraola's Breadcrumb Area End Here -->
        <?php
          $productReview = Review::where('product_id', $product->id)->get();
          $oneOutOfFive = Review::where(['rating'=> 1, 'product_id'=> $product->id])->count('id');
          $twoOutOfFive = Review::where(['rating'=> 2, 'product_id'=> $product->id])->count('id');
          $threeOutOfFive = Review::where(['rating'=> 3, 'product_id'=> $product->id])->count('id');
          $fourOutOfFive = Review::where(['rating'=> 4, 'product_id'=> $product->id])->count('id');
          $fiveOutOfFive = Review::where(['rating'=> 5, 'product_id'=> $product->id])->count('id');
          $averageRating = 0;
          if($oneOutOfFive > 0 || $twoOutOfFive > 0 || $threeOutOfFive || $fourOutOfFive > 0 || $fiveOutOfFive > 0)
          $averageRating = (5*$fiveOutOfFive + 4*$fourOutOfFive + 3*$threeOutOfFive + 2*$twoOutOfFive + 1*$oneOutOfFive) / ($fiveOutOfFive + $fourOutOfFive + $threeOutOfFive + $twoOutOfFive + $oneOutOfFive);
        ?>
        <!-- Begin Hiraola's Single Product Area -->
        <div class="sp-area">
            <div class="container">
                <div class="sp-nav">
                    <div class="row">
                        <div class="col-lg-5 col-md-5">
                            <div class="sp-img_area">
                                <div class="zoompro-border">
                                    <img class="zoompro" src="{{url('/')}}/{{$product->image1}}" data-zoom-image="{{url('/')}}/{{$product->image1}}" alt="Hiraola's Product Image" />
                                </div>
                                <div id="gallery" class="sp-img_slider">
                                    <a class="active" data-image="{{url('/')}}/{{$product->image1}}" data-zoom-image="{{url('/')}}/{{$product->image1}}">
                                        <img src="{{url('/')}}/{{$product->image1}}" alt="Hiraola's Product Image">
                                    </a>
                                    <a data-image="{{url('/')}}/{{$product->image2}}" data-zoom-image="{{url('/')}}/{{$product->image2}}">
                                        <img src="{{url('/')}}/{{$product->image2}}" alt="Hiraola's Product Image">
                                    </a>
                                    <a data-image="{{url('/')}}/{{$product->image3}}" data-zoom-image="{{url('/')}}/{{$product->image3}}">
                                        <img src="{{url('/')}}/{{$product->image3}}" alt="Hiraola's Product Image">
                                    </a>
                                    <a data-image="{{url('/')}}/{{$product->image1}}" data-zoom-image="{{url('/')}}/{{$product->image1}}">
                                        <img src="{{url('/')}}/{{$product->image1}}" alt="Hiraola's Product Image">
                                    </a>
                                    <a data-image="{{url('/')}}/{{$product->image2}}" data-zoom-image="{{url('/')}}/{{$product->image2}}">
                                        <img src="{{url('/')}}/{{$product->image2}}" alt="Hiraola's Product Image">
                                    </a>
                                    <a data-image="{{url('/')}}/{{$product->image3}}" data-zoom-image="{{url('/')}}/{{$product->image3}}">
                                        <img src="{{url('/')}}/{{$product->image3}}" alt="Hiraola's Product Image">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-7 col-md-7">
                            <div class="sp-content">
                                <div class="sp-heading">
                                    <h5><a href="#">{{strtoupper($product->name)}}</a></h5>
                                </div>
                                {{-- <span class="reference">Reference: demo_1</span> --}}
                                <div class="rating-box">
                                    <!-- <ul>
                                        <li><i class="fa fa-star-of-david"></i></li>
                                        <li><i class="fa fa-star-of-david"></i></li>
                                        <li><i class="fa fa-star-of-david"></i></li>
                                        <li><i class="fa fa-star-of-david"></i></li>
                                        <li class="silver-color"><i class="fa fa-star-of-david"></i></li>
                                    </ul> -->
                                    @if($product->Rating)
                                      <span class="fa fa-star {{$product->Rating >= 1 ? 'star-checked' : ''}}"></span>
                                      <span class="fa fa-star {{$product->Rating >= 2 ? 'star-checked' : ''}}"></span>
                                      <span class="fa fa-star {{$product->Rating >= 3 ? 'star-checked' : ''}}"></span>
                                      <span class="fa fa-star {{$product->Rating >= 4 ? 'star-checked' : ''}}"></span>
                                      <span class="fa fa-star {{$product->Rating >= 5 ? 'star-checked' : ''}}"></span>
                                    @else
                                    <span class="invisible">Unrated</span>
                                    @endif
                                </div>
                                <div class="sp-essential_stuff">
                                    <ul>
                                        <li>Price: <a href="javascript:void(0)"><span>Rs. {{$product->sell_price}}</span></a></li>
                                        @if(!empty($product->product_brand))
                                        <li>Brands <a href="javascript:void(0)">{{strtoupper($product->product_brand)}}</a></li>
                                        @endif
                                        {{-- <li>Product Code: <a href="javascript:void(0)">Product 16</a></li> --}}
                                        {{-- <li>Reward Points: <a href="javascript:void(0)">600</a></li> --}}
                                        <li>Availability: <a href="javascript:void(0)"><?php if($product->availability == 'yes'){echo 'In Stock';} else{echo 'Out of Stock';}?></a></li>
                                    </ul>
                                </div>
                                @if(!empty($product->size))
                                <div class="product-size_box">
                                    <span>Size</span>
                                    <select class="myniceselect nice-select">
                                        <option value="1">S</option>
                                        <option value="2">M</option>
                                        <option value="3">L</option>
                                        <option value="4">XL</option>
                                    </select>
                                </div>
                                @endif
                                <div class="quantity">
                                    <label>Quantity</label>
                                    <div class="cart-plus-minus">
                                        <input class="cart-plus-minus-box" value="1" id="quantity{{$product->id}}" type="text">
                                        <div class="dec qtybutton"><i class="fa fa-angle-down"></i></div>
                                        <div class="inc qtybutton"><i class="fa fa-angle-up"></i></div>
                                    </div>
                                </div>
                                <div class="qty-btn_area">
                                    <ul>
                                        <li><a class="qty-cart_btn" onclick="addToCart({{$product->id}},@if(!empty(Auth::user()->id)) {{Auth::user()->id}} @endif)">Add To Cart</a></li>
                                        <li><a class="qty-wishlist_btn"  data-toggle="tooltip" title="Add To Wishlist" onclick="addToWishlist({{$product->id}})" ><i class="ion-android-favorite-outline"></i></a></li>
                                        {{-- <li><a class="qty-compare_btn" href="" data-toggle="tooltip" title="Compare This Product"><i class="ion-ios-shuffle-strong"></i></a></li> --}}
                                    </ul>
                                </div>
                                {{-- <div class="hiraola-tag-line">
                                    <h6>Tags:</h6>
                                    <a href="javascript:void(0)">Ring</a>,
                                    <a href="javascript:void(0)">Necklaces</a>,
                                    <a href="javascript:void(0)">Braid</a>
                                </div> --}}
                                <div class="hiraola-social_link">
                                    <ul>
                                        <li class="facebook">
                                            <a href="/shareLinkOnSocialSite/fb/{{$product->slug}}/{{$product->id}}" data-toggle="tooltip" target="_blank" title="Facebook">
                                                <i class="fab fa-facebook"></i>
                                            </a>
                                        </li>
                                        <li class="twitter">
                                            <a href="/shareLinkOnSocialSite/tw/{{$product->slug}}/{{$product->id}}" data-toggle="tooltip" target="_blank" title="Twitter">
                                                <i class="fab fa-twitter-square"></i>
                                            </a>
                                        </li>
                                        <!-- <li class="youtube">
                                            <a href="https://www.youtube.com" data-toggle="tooltip" target="_blank" title="Youtube">
                                                <i class="fab fa-youtube"></i>
                                            </a>
                                        </li> -->
                                        <li class="google-plus">
                                            <a href="/shareLinkOnSocialSite/pi/{{$product->slug}}/{{$product->id}}" data-toggle="tooltip" target="_blank" title="Pinterest">
                                                <i class="fab fa-pinterest"></i>
                                            </a>
                                        </li>
                                        <!-- <li class="instagram">
                                            <a href="https://rss.com" data-toggle="tooltip" target="_blank" title="Instagram">
                                                <i class="fab fa-instagram"></i>
                                            </a>
                                        </li> -->
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Hiraola's Single Product Area End Here -->

        <!-- Begin Hiraola's Single Product Tab Area -->
        <div class="hiraola-product-tab_area-2 sp-product-tab_area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="sp-product-tab_nav ">
                            <div class="product-tab">
                                <ul class="nav product-menu">
                                    <li><a class="active" data-toggle="tab" href="#description"><span>Description</span></a>
                                    </li>
                                    {{-- <li><a data-toggle="tab" href="#specification"><span>Specification</span></a></li> --}}
                                    <?php
                                      $totalReviewCount = Review::where('product_id', $product->id)->count('id');
                                    ?>
                                    <li><a data-toggle="tab" href="#reviews"><span>Reviews ({{$totalReviewCount}})</span></a></li>
                                </ul>
                            </div>
                            <div class="tab-content hiraola-tab_content">
                                <div id="description" class="tab-pane active show" role="tabpanel">
                                    <div class="product-description">
                                        {{htmlspecialchars($product->long_descriptions)}}
                                    </div>
                                </div>

                                <div id="specification" class="tab-pane" role="tabpanel">
                                    <table class="table table-bordered specification-inner_stuff">
                                        <tbody>
                                            <tr>
                                                <td colspan="2"><strong>Memory</strong></td>
                                            </tr>
                                        </tbody>
                                        <tbody>
                                            <tr>
                                                <td>test 1</td>
                                                <td>8gb</td>
                                            </tr>
                                        </tbody>
                                        <tbody>
                                            <tr>
                                                <td colspan="2"><strong>Processor</strong></td>
                                            </tr>
                                        </tbody>
                                        <tbody>
                                            <tr>
                                                <td>No. of Cores</td>
                                                <td>1</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                                <div id="reviews" class="tab-pane" role="tabpanel">
                                    <div class="tab-pane active" id="tab-review">
                                      <div id="review" class="form-horizontal">
                                          <table class="table table-striped table-bordered">
                                              <tbody>
                                                @if($productReview)
                                                  @foreach($productReview as $review)
                                                  <tr>
                                                      <td style="width: 50%;"><strong>{{$review->name}}</strong></td>
                                                      <td class="text-right">{{$review->created_at}}</td>
                                                  </tr>
                                                  <tr>
                                                      <td colspan="2">
                                                          <p>{{$review->review}}</p>
                                                          <div class="rating-box">
                                                              <!-- <ul>
                                                                  <li><i class="fa fa-star-of-david"></i></li>
                                                                  <li><i class="fa fa-star-of-david"></i></li>
                                                                  <li><i class="fa fa-star-of-david"></i></li>
                                                                  <li><i class="fa fa-star-of-david"></i></li>
                                                                  <li><i class="fa fa-star-of-david"></i></li>
                                                              </ul> -->
                                                              @if($product->Rating)
                                                                <span class="fa fa-star {{$product->Rating >= 1 ? 'star-checked' : ''}}"></span>
                                                                <span class="fa fa-star {{$product->Rating >= 2 ? 'star-checked' : ''}}"></span>
                                                                <span class="fa fa-star {{$product->Rating >= 3 ? 'star-checked' : ''}}"></span>
                                                                <span class="fa fa-star {{$product->Rating >= 4 ? 'star-checked' : ''}}"></span>
                                                                <span class="fa fa-star {{$product->Rating >= 5 ? 'star-checked' : ''}}"></span>
                                                              @else
                                                              <span class="invisible">Unrated</span>
                                                              @endif
                                                          </div>
                                                      </td>
                                                  </tr>
                                                  @endforeach
                                                @endif
                                              </tbody>
                                          </table>
                                        </div>

                                        <form class="form-horizontal" id="form-review" action="{{route('give-product-rating')}}" method="post" enctype="multipart/form-data">
                                            <h2>Write a review</h2>
                                            <input type="text" name="product_id" hidden value="{{$product->id}}">
                                            <div class="form-group required">
                                                <div class="col-sm-12 p-0">
                                                    <label>Your Name <span class="required">*</span></label>
                                                    <input class="review-input" type="text" name="name" id="con_email" required>
                                                </div>
                                            </div>
                                            <div class="form-group required">
                                                <div class="col-sm-12 p-0">
                                                    <label>Your Email <span class="required">*</span></label>
                                                    <input class="review-input" type="email" name="email" id="con_email" required>
                                                </div>
                                            </div>
                                            <div class="form-group required second-child">
                                                <div class="col-sm-12 p-0">
                                                    <label class="control-label">Share your opinion</label>
                                                    <textarea class="review-textarea" name="review" id="con_message"></textarea>
                                                    <div class="help-block"><span class="text-danger">Note:</span> HTML is not
                                                        translated!</div>
                                                </div>
                                            </div>
                                            <div class="form-group last-child required">
                                                <div class="col-sm-12 p-0">
                                                    <div class="your-opinion">
                                                        <label>Your Rating</label>
                                                        <span>
                                                        <select class="star-rating" name="rating">
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                        </select>
                                                    </span>
                                                    </div>
                                                </div>
                                                <div class="hiraola-btn-ps_right">
                                                    <!-- <a href="javascript:void(0)" class="hiraola-btn hiraola-btn_dark">Continue</a> -->
                                                    <button class="hiraola-btn hiraola-btn_dark" type="submit">Continue</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Hiraola's Single Product Tab Area End Here -->

<?php
$related_products = Product::where('category_id',$product->category_id)->get();
?>

        <!-- Begin Hiraola's Product Area Two -->
        <div class="hiraola-product_area hiraola-product_area-2 section-space_add">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">


                        <div class="hiraola-section_title">
                            <h4>Related Products</h4>
                        </div>

                    </div>
                    <div class="col-lg-12">
                        <div class="hiraola-product_slider-3">

                            @if(!empty($related_products))
                            @foreach($related_products as $product)
                            <!-- Begin Hiraola's Slide Item Area -->
                            <div class="slide-item">
                                <div class="single_product">
                                    <div class="product-img">
                                        <a href="">
                                            <img class="primary-img" src="{{url('/')}}/{{$product->image1}}" alt="Hiraola's Product Image">
                                            <img class="secondary-img" src="{{url('/')}}/{{$product->image2}}" alt="Hiraola's Product Image">
                                        </a>
                                        {{-- <span class="sticker-2">Sale</span> --}}
                                        <div class="add-actions">
                                            <ul>

                                                <li><a class="hiraola-add_cart" onclick="addToCart({{$product->id}})" data-toggle="tooltip" data-placement="top" title="Add To Cart"><i class="ion-bag"></i></a></li>
                                                {{-- <li><a class="hiraola-add_compare" href="" data-toggle="tooltip" data-placement="top" title="Compare This Product"><i
                                                        class="ion-ios-shuffle-strong"></i></a></li> --}}
                                                <li class="quick-view-btn" data-toggle="modal" data-target="#exampleModalCenter" data-myid="{{$product->id}}"><a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="Quick View"><i class="ion-eye"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="hiraola-product_content">
                                        <div class="product-desc_info">
                                            <h6><a class="product-name" href="/product-detail/{{$product->slug}}">{{$product->name}}</a></h6>
                                            <div class="price-box">
                                                <span class="new-price">Rs. {{$product->sell_price}}</span>
                                            </div>
                                            <div class="additional-add_action">
                                                <ul>
                                                    <li><a class="hiraola-add_compare" data-toggle="tooltip" data-placement="top" title="Add To Wishlist" onclick="addToWishlist({{$product->id}})"><i
                                                            class="ion-android-favorite-outline"></i></a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="rating-box">
                                              @if($product->Rating)
                                                <span class="fa fa-star {{$product->Rating >= 1 ? 'star-checked' : ''}}"></span>
                                                <span class="fa fa-star {{$product->Rating >= 2 ? 'star-checked' : ''}}"></span>
                                                <span class="fa fa-star {{$product->Rating >= 3 ? 'star-checked' : ''}}"></span>
                                                <span class="fa fa-star {{$product->Rating >= 4 ? 'star-checked' : ''}}"></span>
                                                <span class="fa fa-star {{$product->Rating >= 5 ? 'star-checked' : ''}}"></span>
                                              @else
                                              <span class="invisible">Unrated</span>
                                              @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            @endif
                            <!-- Hiraola's Slide Item Area End Here -->

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Hiraola's Product Area Two End Here -->

@endsection

@section ('js-script')
    <script>

    function addToCart(id){
        var quantity = $('#quantity'+id).val();
         var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        // alert(id+" / "+quantity );

      $.ajax({
         url: '/add-to-cart',
         type: 'POST',
         data: {_token: CSRF_TOKEN, id: id, quantity: quantity},
         success: function (data) {
           // alert(data.cartItem);
           $('#cartItem').html(data.cartItem)
         },
         failure: function (data) {
           alert(data.message);
         }
      });
    }

    function addToWishlist(id){
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      userId = <?php if(!empty(Auth::user()->id)){echo Auth::user()->id;} else {echo 0;}?> ;

      if(userId == 0){
        alert('Please Login First');
      }
      else{
        // alert(id);

      $.ajax({
         url: '/add-to-wishlist',
         type: 'POST',
         data: {_token: CSRF_TOKEN, id: id, userId : userId},
         success: function (data) {
           // alert(data.wishItem);
           $('#wishItem').html(data.wishItem);
         },
         failure: function (data) {
           alert(data);
         }
      });
      }
   }
    </script>

@endsection
