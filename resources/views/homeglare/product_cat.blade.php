<div class="shop-product-wrap grid gridview-3 row" id="productCat">
                    @if($searched_products != null)
                    @foreach($searched_products as $product)
                    <div class="col-lg-4">
                        <div class="slide-item">
                            <div class="single_product">
                                <div class="product-img">
                                    <a href="/product-detail/{{$product->slug}}">
                                        <img class="primary-img" src="{{url('/')}}/{{$product->image1}}" alt="Hiraola's Product Image" style="width: 438px; height: 238px;">
                                        <img class="secondary-img" src="{{url('/')}}/{{$product->image2}}" alt="Hiraola's Product Image" style="width: 438px; height: 238px;">
                                    </a>
                                    <div class="add-actions">
                                        <ul>
                                            <li><a class="hiraola-add_cart" data-toggle="tooltip" data-placement="top" title="Add To Cart" onclick="addToCart({{$product->id}})"><i class="ion-bag"></i></a>
                                            </li>
                                            {{-- <li><a class="hiraola-add_compare" href="" data-toggle="tooltip" data-placement="top" title="Compare This Product"><i
                                                class="ion-ios-shuffle-strong"></i></a>
                                            </li> --}}
                                            <li class="quick-view-btn" data-toggle="modal" data-target="#exampleModalCenter" data-myid="{{$product->id}}"><a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="Quick View"><i
                                                class="ion-eye"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="hiraola-product_content">
                                        <div class="product-desc_info">
                                            <h6><a class="product-name" href="/product-detail/{{$product->slug}}">{{strtoupper(substr($product->name,0,18))}} <?php strlen($product->name)>18 ? "...":null; ?></a></h6>
                                            <div class="price-box">
                                                <span class="new-price">Rs. {{$product->sell_price}}</span>
                                            </div>
                                            <div class="additional-add_action">
                                                <ul>
                                                    <li><a class="hiraola-add_compare" data-toggle="tooltip" data-placement="top" title="Add To Wishlist" onclick="addToWishlist({{$product->id}})"><i
                                                        class="ion-android-favorite-outline"></i></a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="rating-box">
                                                <!-- <ul>
                                                    <li><i class="fa fa-star-of-david"></i></li>
                                                    <li><i class="fa fa-star-of-david"></i></li>
                                                    <li><i class="fa fa-star-of-david"></i></li>
                                                    <li><i class="fa fa-star-of-david"></i></li>
                                                    <li class="silver-color"><i class="fa fa-star-of-david"></i></li>
                                                </ul> -->
                                                @if($product->Rating)
                                                  <span class="fa fa-star {{$product->Rating >= 1 ? 'star-checked' : ''}}"></span>
                                                  <span class="fa fa-star {{$product->Rating >= 2 ? 'star-checked' : ''}}"></span>
                                                  <span class="fa fa-star {{$product->Rating >= 3 ? 'star-checked' : ''}}"></span>
                                                  <span class="fa fa-star {{$product->Rating >= 4 ? 'star-checked' : ''}}"></span>
                                                  <span class="fa fa-star {{$product->Rating >= 5 ? 'star-checked' : ''}}"></span>
                                                @else
                                                <span class="invisible">Unrated</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="list-slide_item">
                                <div class="single_product">
                                    <div class="product-img">
                                        <a href="/product-detail/{{$product->slug}}">
                                            <img class="primary-img" src="{{url('/')}}/{{$product->image1}}" alt="Hiraola's Product Image" style="width: 438px; height: 230px;">
                                            <img class="secondary-img" src="{{url('/')}}/{{$product->image2}}" alt="Hiraola's Product Image" style="width: 438px; height: 230px;">
                                        </a>
                                    </div>
                                    <div class="hiraola-product_content">
                                        <div class="product-desc_info">
                                            <h6><a class="product-name" href="/product-detail/{{$product->slug}}">{{strtoupper(substr($product->name,0,25))}} <?php strlen($product->name)>25 ? "...":null; ?></a></h6>
                                            <div class="rating-box">
                                                <!-- <ul>
                                                    <li><i class="fa fa-star-of-david"></i></li>
                                                    <li><i class="fa fa-star-of-david"></i></li>
                                                    <li><i class="fa fa-star-of-david"></i></li>
                                                    <li><i class="fa fa-star-of-david"></i></li>
                                                    <li class="silver-color"><i class="fa fa-star-of-david"></i></li>
                                                </ul> -->
                                                @if($product->Rating)
                                                  <span class="fa fa-star {{$product->Rating >= 1 ? 'star-checked' : ''}}"></span>
                                                  <span class="fa fa-star {{$product->Rating >= 2 ? 'star-checked' : ''}}"></span>
                                                  <span class="fa fa-star {{$product->Rating >= 3 ? 'star-checked' : ''}}"></span>
                                                  <span class="fa fa-star {{$product->Rating >= 4 ? 'star-checked' : ''}}"></span>
                                                  <span class="fa fa-star {{$product->Rating >= 5 ? 'star-checked' : ''}}"></span>
                                                @else
                                                <span class="invisible">Unrated</span>
                                                @endif
                                            </div>
                                            <div class="price-box">
                                                <span class="new-price">Rs. {{$product->sell_price}}</span>
                                            </div>
                                            <div class="product-short_desc">
                                                <p>{{$product->short_descriptions}}</p>
                                            </div>
                                        </div>
                                        <div class="add-actions">
                                            <ul>
                                                <li><a class="hiraola-add_cart" data-toggle="tooltip" data-placement="top" title="Add To Cart" onclick="addToCart({{$product->id}})">Add To Cart</a></li>
                                                {{-- <li><a class="hiraola-add_compare" href="" data-toggle="tooltip" data-placement="top" title="Compare This Product"><i
                                                    class="ion-ios-shuffle-strong"></i></a></li> --}}
                                                    <li class="quick-view-btn" data-toggle="modal" data-target="#exampleModalCenter" data-myid="{{$product->id}}"><a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="Quick View"><i
                                                        class="ion-eye"></i></a></li>
                                                        <li><a class="hiraola-add_compare" data-toggle="tooltip" data-placement="top" title="Add To Wishlist" onclick="addToWishlist({{$product->id}})"><i
                                                            class="ion-android-favorite-outline"></i></a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                                @endif

                            </div>
