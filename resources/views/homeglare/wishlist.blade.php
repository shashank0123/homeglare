<?php
use App\Models\Product;
?>


@extends('layouts/ecommerce')

@section('content')
<style>
    #img-response{ width: 100px; height: 100px }
</style>


<!-- Begin Hiraola's Breadcrumb Area -->
<!-- <div class="breadcrumb-area">
    <div class="container">
        <div class="breadcrumb-content">
            <h2>Other</h2>
            <ul>
                <li><a href="/">Home</a></li>
                <li class="active">Wishlist</li>
            </ul>
        </div>
    </div>
</div> -->
<!-- Hiraola's Breadcrumb Area End Here -->
<!--Begin Hiraola's Wishlist Area -->
<div class="hiraola-wishlist_area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <form action="javascript:void(0)">
                    <div class="table-content table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th class="hiraola-product_remove">remove</th>
                                    <th class="hiraola-product-thumbnail">images</th>
                                    <th class="cart-product-name">Product</th>
                                    <th class="hiraola-product-price">Unit Price</th>
                                    <th class="hiraola-product-price">Quantity</th>
                                    <th class="hiraola-product-stock-status">Stock Status</th>
                                    <th class="hiraola-cart_btn">add to cart</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(!empty($wishlist))
                                @foreach($wishlist as $list)
                                <?php $item = Product::where('id',$list->product_id)->first(); ?>
                                <tr id="div-list{{$list->id}}">
                                    <td class="hiraola-product_remove"><a onclick="deleteWishlistItem({{$list->id}})"><i class="fa fa-trash"
                                        title="Remove"></i></a></td>
                                        <td class="hiraola-product-thumbnail"><a href="/product-detail/{{$item->slug}}"><img src="{{url('/')}}/{{$item->image1}}" alt="Hiraola's Wishlist Thumbnail" id="img-response"></a>
                                        </td>
                                        <td class="hiraola-product-name"><a href="javascript:void(0)">{{$item->name}}</a></td>
                                        <td class="hiraola-product-price"><span class="amount">Rs. {{ $item->sell_price }}</span></td>
                                        <input type="hidden" value="{{$list->id}}" id="listId">
                                        <td class="hiraola-product-price">
                                            <span class="amount">
                                                <i class="fa fa-minus" onclick="decrement({{$list->id}})"></i>&nbsp;
                                                <input type="text" name="quantity" id="quantity{{$item->id}}" value="1" style="width: 40px; text-align: center;" />&nbsp;
                                                <i class="fa fa-plus" onclick="increment({{$list->id}})"></i>
                                            </span>
                                        </td>

                                        <td class="hiraola-product-stock-status"><span class="in-stock">{{$item->availability}}</span></td>
                                        <td class="hiraola-cart_btn"><a onclick="addToCart({{$item->id}},{{$list->id}})">add to cart</a></td>
                                    </tr>
                                    @endforeach
                                    @else
                                    {{'No Products in your Wishlist'}}
                                    @endif

                                </tbody>
                            </table>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Hiraola's Wishlist Area End Here -->

    @endsection

    @section('js-script')

    <script>

        function decrement(id){

            // alert(id);
            var value = $('#quantity'+id).val();
            if(value > 1){
                value--;
            }

            // alert(value);
            $('#quantity'+id).val(value);
        }

        function increment(id){

            // alert(id);
            var value = $('#quantity'+id).val();
            value++;
            // alert(value);
            $('#quantity'+id).val(value);
        }


        function addToCart(id,list){
          var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
          var quantity = $('#quantity'+id).val();
          // alert(id+" / "+quantity);

          $.ajax({
             url: '/add-to-cart',
             type: 'POST',
             data: {_token: CSRF_TOKEN, id: id, quantity: quantity},
             success: function (data) {
               // alert(data.cartItem);
               $('#cartItem').html(data.cartItem);

           },
           failure: function (data) {
               // alert(data.message);
           }
       });
      }

      function deleteWishlistItem(id){
          var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
          user_id = {{Auth::user()->id}};
          // alert(id+" / "+user_id);

          $.ajax({
             url: '/delete-wishlist-item',
             type: 'POST',
             data: {_token: CSRF_TOKEN, id: id, user_id : user_id},
             success: function (data) {
               // alert(data.flag);
               $('#cartItem').html(data.cartItem);
               $('#wishItem').html(data.wishItem);

                $('#div-list'+id).hide();
           },
           failure: function (data) {
               // alert(data.message);
           }
       });
      }
  </script>

  @endsection
