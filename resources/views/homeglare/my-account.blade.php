@extends('layouts/ecommerce')

@section('content')


<!-- Begin Hiraola's Breadcrumb Area -->
<!-- <div class="breadcrumb-area">
    <div class="container">
        <div class="breadcrumb-content">
            <h2>Other</h2>
            <ul>
                <li><a href="/">Home</a></li>
                <li class="active">My Account</li>
            </ul>
        </div>
    </div>
</div> -->
<!-- Hiraola's Breadcrumb Area End Here -->
<!-- Begin Hiraola's Page Content Area -->
<main class="page-content">
    <!-- Begin Hiraola's Account Page Area -->
    <div class="account-page-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <ul class="nav myaccount-tab-trigger" id="account-page-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="account-dashboard-tab" data-toggle="tab" href="#account-dashboard" role="tab" aria-controls="account-dashboard" aria-selected="true">Dashboard</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="account-orders-tab" data-toggle="tab" href="#account-orders" role="tab" aria-controls="account-orders" aria-selected="false">Orders</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="account-address-tab" data-toggle="tab" href="#account-address" role="tab" aria-controls="account-address" aria-selected="false">Addresses</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="account-help-tab" data-toggle="tab" href="#account-help" role="tab" aria-controls="account-help" aria-selected="false">Help & Support</a>
                        </li>
                        <!-- <li class="nav-item">
                            <a class="nav-link" id="account-details-tab" data-toggle="tab" href="#account-details" role="tab" aria-controls="account-details" aria-selected="false">Account Details</a>
                        </li> -->
                        <li class="nav-item">
                          <a class="nav-link" id="account-logout-tab" href="{{ route('logout') }}"
                          onclick="event.preventDefault();
                          document.getElementById('logout-form').submit();" role="tab" aria-selected="false">Logout</a>
                          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                              @csrf
                          </form>
                      </li>
                  </ul>
              </div>
              <div class="col-lg-9">
                <div class="tab-content myaccount-tab-content" id="account-page-tab-content">
                    <div class="tab-pane fade  active" id="account-dashboard" role="tabpanel" aria-labelledby="account-dashboard-tab">
                        <div class="myaccount-dashboard">
                            <p>Hello <b>{{$user->name}}</b></p>
                            <!-- <p>From your account dashboard you can view your recent orders, manage your shipping and
                                billing addresses and <a href="javascript:void(0)">edit your password and account details</a>.</p>
                            </div> -->
                        </div>
                      </div>
                        <div class="tab-pane fade" id="account-orders" role="tabpanel" aria-labelledby="account-orders-tab">
                            <div class="myaccount-orders">
                                <h4 class="small-title">MY ORDERS</h4>
                                <div class="table-responsive">
                                  @if($orderDetails)
                                    <table class="table table-bordered table-hover">
                                      <thead>
                                        <tr>
                                            <th>ORDER ID</th>
                                            <th>DATE</th>
                                            <th>ADDRESS</th>
                                            <th>STATUS</th>
                                            <th>TOTAL</th>
                                            <th colspan="2">Action</th>
                                        </tr>
                                      </thead>
                                        <tbody>
                                          @foreach($orderDetails as $order)
                                            <tr>
                                                <td><a class="account-order-id" href="javascript:void(0)">{{$order->id}}</a></td>
                                                <td>{{$order->created_at->format('d-m-Y')}}</td>
                                                <td>{{str_limit($order->address, 20)}}</td>
                                                <td>{{str_replace('_', ' ', $order->status)}}</td>
                                                <td>Rs. {{$order->price}} for {{$order->ItemCount}} items</td>
                                                <td>
                                                  <a href="/ordered-item-details/{{$order->id}}" class="hiraola-btn hiraola-btn_dark hiraola-btn_sm"><span>View</span></a>
                                                </td>
                                                <td>
                                                  <a href="/ordered-item-track/{{$order->id}}" class="hiraola-btn hiraola-btn_dark hiraola-btn_sm"><span>Track</span></a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="account-address" role="tabpanel" aria-labelledby="account-address-tab">
                            <div class="myaccount-address">
                                <p>The following addresses will be used on the checkout page by default.</p>
                                <div class="row">
                                    <div class="col">
                                        <h4 class="small-title">Billing Address</h4>
                                        <address>
                                            <!--  1234 Heaven Stress, Beverly Hill OldYork UnitedState of Lorem -->
                                            @if($address)
                                              {{$address->address}}, {{$address->city}}, {{$address->state}}, {{$address->country}} - {{$address->pin_code}}<br>
                                            @if(!empty($address->phone))
                                              Contact - {{$address->phone}}
                                              @endif
                                            @else
                                            <span> </span>
                                            @endif
                                        </address>
                                    </div>
                                    <div class="col">
                                        <h4 class="small-title">Shipping Address</h4>
                                        <address>
                                            <ul>
                                          @if(!empty($addresses))
                                          @foreach($addresses as $address)
                                          <li> -> 
                                            {{$address->address}}, {{$address->city}}, {{$address->state}}, {{$address->country}} - {{$address->pin_code}}<br>
                                            @if(!empty($address->phone))
                                              Contact - {{$address->phone}}
                                              @endif
                                          </li>
                                              
                                              @endforeach
                                          @else
                                          <ul>
                                          <span> </span>
                                          @endif
                                        </address>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="account-help" role="tabpanel" aria-labelledby="account-help-tab">
                            <div class="myaccount-help">
                              <div class="row mb-3">
                                <div class="col-4">
                                  <a href="/help-support/create" class="hiraola-btn hiraola-btn_dark"><span>Add Query</span></a>
                                </div>
                                <div class="col-8">
                                  <h4 class="small-title">HELP & SUPPORT</h4>
                                </div>
                              </div>
                                <div class="table-responsive">
                                  @if($help)
                                    <table class="table table-bordered table-hover">
                                      <thead>
                                        <tr>
                                            <th>Token</th>
                                            <th>Subject</th>
                                            <th>Message</th>
                                            <th>Date</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                      </thead>
                                        <tbody>
                                          @foreach($help as $item)
                                            <tr>
                                              <td><a class="account-help-token" href="javascript:void(0)">{{$item->token}}</a></td>
                                                <td>{{$item->subject}}</td>
                                                <td>{{$item->message}}</td>
                                                <td>{{$item->created_at->format('d-m-Y') }}</td>
                                                <td>{{$item->status}}</td>
                                                <td>
                                                  <a href="/reply?token={{$item->token}}" class="hiraola-btn hiraola-btn_dark hiraola-btn_sm"><span>Reply</span></a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="account-details" role="tabpanel" aria-labelledby="account-details-tab">
                            <div class="myaccount-details">
                                <form action="#" class="hiraola-form">
                                    <div class="hiraola-form-inner">
                                        <div class="single-input single-input-half">
                                            <label for="account-details-firstname">First Name*</label>
                                            <input type="text" id="account-details-firstname">
                                        </div>
                                        <div class="single-input single-input-half">
                                            <label for="account-details-lastname">Last Name*</label>
                                            <input type="text" id="account-details-lastname">
                                        </div>
                                        <div class="single-input">
                                            <label for="account-details-email">Email*</label>
                                            <input type="email" id="account-details-email">
                                        </div>
                                        <div class="single-input">
                                            <label for="account-details-oldpass">Current Password(leave blank to leave
                                            unchanged)</label>
                                            <input type="password" id="account-details-oldpass">
                                        </div>
                                        <div class="single-input">
                                            <label for="account-details-newpass">New Password (leave blank to leave
                                            unchanged)</label>
                                            <input type="password" id="account-details-newpass">
                                        </div>
                                        <div class="single-input">
                                            <label for="account-details-confpass">Confirm New Password</label>
                                            <input type="password" id="account-details-confpass">
                                        </div>
                                        <div class="single-input">
                                            <button class="hiraola-btn hiraola-btn_dark" type="submit"><span>SAVE
                                            CHANGES</span></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Hiraola's Account Page Area End Here -->
</main>
<!-- Hiraola's Page Content Area End Here -->

@endsection
