<?php
use App\Models\Category;
use App\Models\Product;
use App\Models\Review;
use App\Models\Banner;

$maincategory = Category::where('status','Active')->where('category_id',0)->get();
$slider_banners = Banner::where('image_type','slider')->where('status','Active')->get();

$portrait_banner = Banner::where('image_type','portrait')->where('status','Active')->limit(1)->first();
$medium_banners = Banner::where('image_type','medium')->where('status','Active')->limit(3)->get();

$full_banner = Banner::where('image_type','medium')->where('status','Active')->limit(1)->first();
$small_banners = Banner::where('image_type','small')->where('status','Active')->limit(2)->get();
$small_banners2 = Banner::where('image_type','small')->where('status','Active')->orderBy('created_at','DESC')->limit(3)->get();
?>


@extends('layouts/ecommerce')

@section('content')
<style>
    @if(!empty($slider_banners))
    <?php $i=1;
    ?>
      @foreach($slider_banners as $banner)
        .bg-{{$i++}} {background-image: url("{{$banner->image_url}}");}
      @endforeach
    @endif

    @if(!empty($full_banner))
      .static-banner_area .static-banner-image{ background-image: url("{{$full_banner->image_url}}"); }
    @endif

/*.bg-2 {background-image: url("banners/3ae6fa85a1800b207fc5dbbc3cbe9e93.png");}
.bg-3 {background-image: url("banners/3ae6fa85a1800b207fc5dbbc3cbe9e93.png");}*/
</style>

        <div class="slider-with-category_menu">
            <div class="container">
                <div class="row">
                    <div class="col grid-half order-md-2 order-lg-1">
                        <div class="category-menu">
                            <div class="category-heading">
                                <h2 class="categories-toggle"><span>Shop by categories</span></h2>
                            </div>
                            <div id="cate-toggle" class="category-menu-list">
                                <ul>
                                    @foreach($maincategory as $row)
                            <li class="right-menu rx-parent"><a href="/products/{{$row->slug}}">{{$row->category_name}}</a>
                                <?php $category=Category::where('category_id',$row->id)->get(); ?>
                                <ul class="cat-mega-menu">
                                    @foreach($category as $col)
                                    <li class="right-menu cat-mega-title">
                                        <a href="/products/{{$col->slug}}">{{ $col->category_name }}</a>
                                        <?php $subproduct=Category::where('category_id',$col->id)->get(); ?>
                                        <ul>
                                            @foreach($subproduct as $sets)
                                            <li><a href="/products/{{$sets->slug}}">{{$sets->category_name}}</a></li>
                                            @endforeach
                                        </ul>
                                    </li>
                                    @endforeach
                                </ul>
                            </li>
                            @endforeach

                            <li class="rx-parent">
                                <a class="rx-default">More Categories</a>
                                <a class="rx-show">Collapse</a>
                            </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col grid-full order-md-1 order-lg-2">
                        <div class="hiraola-slider_area">
                            <div class="main-slider">
                                <!-- Begin Single Slide Area -->
                                @if(!empty($slider_banners))
                                <?php $i=1; ?>
                                @foreach($slider_banners as $banner)
                                <div class="single-slide animation-style-01 bg-{{$i++}}">
                                    <div class="container">
                                        <div class="slider-progress"></div>
                                    </div>
                                </div>
                                @endforeach
                                @endif
                                <!-- Single Slide Area End Here -->
                                <!-- Begin Single Slide Area -->
                            </div>
                        </div>
                    </div>
                    <div class="col grid-half grid-md_half order-md-2 order-lg-3">
                        <div class="banner-item img-hover_effect">
                             @if(!empty($portrait_banner))
                              <a href="">
                                 <img class="img-full" src="{{url("/")}}/{{$portrait_banner->image_url}}" alt="Homeglare Portrait Banner">
                              </a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
<!-- medium banners -->
        <div class="hiraola-banner_area">
            <div class="container">
                <div class="row">
                    @if(!empty($medium_banners))
                      @foreach($medium_banners as $banner)
                        <div class="col-lg-4">
                            <div class="banner-item img-hover_effect">
                                <a href="">
                                    <img class="img-full" src="{{url('/')}}/{{$banner->image_url}}" alt="Homeglare Medium Banner">
                                </a>
                            </div>
                        </div>
                      @endforeach
                    @endif
                </div>
            </div>
        </div>

        <?php
        $new_all = Product::where(['status' => 'Active', 'availability' => 'yes'])->orderBy('created_at','DESC')->limit(15)->get();
        ?>

        <!-- Begin Homeglare Product Area -->
        <div class="hiraola-product_area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="hiraola-section_title">
                            <h4>New Arrival</h4>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="hiraola-product_slider">
                            <!-- Begin Homeglare Slide Item Area -->
                            @foreach($new_all as $new)
                            <div class="slide-item">
                                <div class="single_product">
                                    <div class="product-img">
                                        {{-- Overall image size shoulb be 438*438 --}}
                                        <a href="/product-detail/{{$new->slug}}">
                                            <img class="primary-img" src="{{url('/')}}/{{$new->image1}}" alt="Homeglare Product Image" style="width: 438px; height: 238px">
                                            <img class="secondary-img" src="{{url('/')}}/{{$new->image2}}" alt="Homeglare Product Image" style="width: 438px; height: 238px">
                                        </a>
                                        {{-- <span class="sticker">New</span> --}}
                                        <div class="add-actions">
                                            <ul>
                                                <li><a class="hiraola-add_cart" onclick="addToCart({{$new->id}})" data-toggle="tooltip" data-placement="top" title="Add To Cart"><i class="ion-bag"></i></a></li>
                                                <li class="quick-view-btn" data-toggle="modal" data-target="#exampleModalCenter" data-myid="{{$new->id}}"><a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="Quick View"><i class="ion-eye"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="hiraola-product_content">
                                        <div class="product-desc_info">
                                            <h6><a class="product-name"  href="/product-detail/{{$new->slug}}">{{str_limit($new->name,  18)}}</a></h6>
                                            <div class="price-box">
                                                <span class="new-price">Rs. {{$new->sell_price}}</span>
                                            </div>
                                            <div class="additional-add_action">
                                                <ul>
                                                    <li><a class="hiraola-add_compare" onclick="addToWishlist({{$new->id}})" data-toggle="tooltip" data-placement="top" title="Add To Wishlist"><i class="ion-android-favorite-outline"></i></a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="rating-box">
                                                @if($new->Rating)
                                                  <span class="fa fa-star {{$new->Rating >= 1 ? 'star-checked' : ''}}"></span>
                                                  <span class="fa fa-star {{$new->Rating >= 2 ? 'star-checked' : ''}}"></span>
                                                  <span class="fa fa-star {{$new->Rating >= 3 ? 'star-checked' : ''}}"></span>
                                                  <span class="fa fa-star {{$new->Rating >= 4 ? 'star-checked' : ''}}"></span>
                                                  <span class="fa fa-star {{$new->Rating >= 5 ? 'star-checked' : ''}}"></span>
                                                  @else
                                                  <span class="invisible">Unrated</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            <!-- Homeglare Slide Item Area End Here -->

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Homeglare Product Area End Here -->

        <?php
        $hot_sale = Product::where(['status' => 'Active', 'availability' => 'yes'])->inRandomOrder()->limit(10)->get();
        ?>
        <!-- Begin Homeglare Product Area -->
        <div class="hiraola-product_area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="hiraola-section_title">
                            <h4>Hot Sale</h4>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="hiraola-product_slider">
                            <!-- Begin Homeglare Slide Item Area -->
                            @foreach($hot_sale as $new)
                            <div class="slide-item">
                                <div class="single_product">
                                    <div class="product-img">
                                        {{-- Overall image size shoulb be 438*438 --}}
                                        <a href="/product-detail/{{$new->slug}}">
                                            <img class="primary-img" src="{{url('/')}}/{{$new->image1}}" alt="Homeglare Product Image" style="width: 438px; height: 238px">
                                            <img class="secondary-img" src="{{url('/')}}/{{$new->image2}}" alt="Homeglare Product Image" style="width: 438px; height: 238px">
                                        </a>
                                        <span class="sticker">New</span>
                                        <div class="add-actions">
                                            <ul>
                                                <li><a class="hiraola-add_cart"  onclick="addToCart({{$new->id}})" data-toggle="tooltip" data-placement="top" title="Add To Cart"><i class="ion-bag"></i></a></li>
                                                <li class="quick-view-btn" data-toggle="modal" data-target="#exampleModalCenter" data-myid="{{$new->id}}"><a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="Quick View"><i class="ion-eye"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="hiraola-product_content">
                                        <div class="product-desc_info">
                                            <h6><a class="product-name"  href="/product-detail/{{$new->slug}}">{{str_limit($new->name, 18)}}</a></h6>
                                            <div class="price-box">
                                                <span class="new-price">Rs. {{$new->sell_price}}</span>
                                            </div>
                                            <div class="additional-add_action">
                                                <ul>
                                                    <li><a class="hiraola-add_compare"  onclick="addToWishlist({{$new->id}})" data-toggle="tooltip" data-placement="top" title="Add To Wishlist"><i class="ion-android-favorite-outline"></i></a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="rating-box">
                                              @if($new->Rating)
                                                <span class="fa fa-star {{$new->Rating >= 1 ? 'star-checked' : ''}}"></span>
                                                <span class="fa fa-star {{$new->Rating >= 2 ? 'star-checked' : ''}}"></span>
                                                <span class="fa fa-star {{$new->Rating >= 3 ? 'star-checked' : ''}}"></span>
                                                <span class="fa fa-star {{$new->Rating >= 4 ? 'star-checked' : ''}}"></span>
                                                <span class="fa fa-star {{$new->Rating >= 5 ? 'star-checked' : ''}}"></span>
                                                @else
                                                <span class="invisible">Unrated</span>
                                              @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            <!-- Homeglare Slide Item Area End Here -->

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Homeglare Product Area End Here -->
        <!-- small banner -->
        <div class="hiraola-banner_area-2">
            <div class="container">
                <div class="row">
                    @if(!empty($small_banners))
                    @foreach($small_banners as $banner)
                    <div class="col-lg-6">
                        <div class="banner-item img-hover_effect">
                            <a href="">
                                <img class="img-full" src="{{url('/')}}/{{$banner->image_url}}" alt="Homeglare Banner">
                            </a>
                        </div>
                    </div>
                    @endforeach
                    @endif
                </div>
            </div>
        </div>


        <?php
        $main_cat = Category::where('category_id',0)->where('status','Active')->get();
        $count_main = 0;
        $count_pro = 0;
        $products_array = array();
        $count = 0;

        $all_pro = Product::where(['status' => 'Active', 'availability' => 'yes'])->inRandomOrder(10)->get();
        ?>

        <!-- Begin Homeglare Product Tab Area Two -->
        <div class="hiraola-product-tab_area-3">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="product-tab">
                            <ul class="nav product-menu">

                                @foreach($main_cat as $main)
                                <li><a class="<?php if($count_main == 0){ echo 'active'; $count_main++; } ?>" data-toggle="tab" href="#{{$main->slug}}"><span>{{$main->category_name}}</span></a></li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="tab-content hiraola-tab_content">
                            @foreach($main_cat as $main)

                            <div id="{{$main->slug}}" class="tab-pane <?php if($count_pro == 0){ echo 'active'; $count_pro++; } ?> show" role="tabpanel">
                                <div class="hiraola-product-tab_slider-3">
                                    <!-- Begin Homeglare Slide Item Area -->
                                    @foreach($all_pro as $pro)
                                    <div class="slide-item">
                                        <div class="single_product">
                                            <div class="product-img">
                                                <a href="/product-detail/{{$pro->slug}}">
                                                    <img class="primary-img" src="{{url('/')}}/{{$pro->image1}}" alt="Homeglare Product Image" style="width: 438px; height: 238px">
                                                    <img class="secondary-img" src="{{url('/')}}/{{$pro->image2}}" alt="Homeglare Product Image" style="width: 438px; height: 238px">
                                                </a>
                                                <div class="add-actions">
                                                    <ul>
                                                        <li><a class="hiraola-add_cart"  onclick="addToCart({{$pro->id}})" data-toggle="tooltip" data-placement="top" title="Add To Cart"><i class="ion-bag"></i></a>
                                                        </li>
                                                        <li class="quick-view-btn" data-toggle="modal" data-target="#exampleModalCenter" data-myid="{{$pro->id}}"><a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="Quick View"><i class="ion-eye"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="hiraola-product_content">
                                                <div class="product-desc_info">
                                                    <h6><a class="product-name" href="/product-detail/{{$pro->slug}}">{{str_limit($pro->name, 18)}}</a></h6>
                                                    <div class="price-box">
                                                        <span class="new-price">Rs. {{$pro->sell_price}}</span>
                                                    </div>
                                                    <div class="additional-add_action">
                                                        <ul>
                                                            <li><a class="hiraola-add_compare"  onclick="addToWishlist({{$pro->id}})" data-toggle="tooltip" data-placement="top" title="Add To Wishlist"><i
                                                                    class="ion-android-favorite-outline"></i></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="rating-box">
                                                      @if($pro->Rating)
                                                        <span class="fa fa-star {{$pro->Rating >= 1 ? 'star-checked' : ''}}"></span>
                                                        <span class="fa fa-star {{$pro->Rating >= 2 ? 'star-checked' : ''}}"></span>
                                                        <span class="fa fa-star {{$pro->Rating >= 3 ? 'star-checked' : ''}}"></span>
                                                        <span class="fa fa-star {{$pro->Rating >= 4 ? 'star-checked' : ''}}"></span>
                                                        <span class="fa fa-star {{$pro->Rating >= 5 ? 'star-checked' : ''}}"></span>
                                                      @else
                                                      <span class="invisible">Unrated</span>
                                                      @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                    <!-- Homeglare Slide Item Area End Here -->
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Homeglare Product Tab Area TwoEnd Here -->







        <div class="hiraola-banner_area-3">
            <div class="container">
                <div class="row">
                    @if(!empty($small_banners2))
                    @foreach($small_banners2 as $banner)
                    <div class="col-lg-4">
                        <div class="banner-item img-hover_effect">
                            <a href="">
                                <img class="img-full" src="{{url('/')}}/{{$banner->image_url}}" alt="Homeglare Banner">
                            </a>
                        </div>
                    </div>
                    @endforeach
                    @endif
                </div>
            </div>
        </div>


        <?php $featured = Product::where(['status' => 'Active', 'availability' => 'yes', 'trending'=>'yes'])->orderBy('created_at','DESC')->inRandomOrder(15)->get(); ?>

        <!-- Begin Homeglare Product Tab Area Three -->
        <div class="hiraola-product-tab_area-4">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="product-tab">
                            <div class="hiraola-tab_title">
                                <h4>Trending Products</h4>
                            </div>

                        </div>
                        <div class="tab-content hiraola-tab_content">
                            <div id="necklaces-2" class="tab-pane active show" role="tabpanel">
                                <div class="hiraola-product-tab_slider-2">
                                    <!-- Begin Homeglare Slide Item Area -->
                                    @if(!empty($featured))
                                    @foreach($featured as $product)
                                    <div class="slide-item">
                                        <div class="single_product">
                                            <div class="product-img">
                                                <a href="/product-detail/{{$product->slug}}"href="">
                                                    <img class="primary-img" src="{{url('/')}}/{{$product->image1}}" alt="Homeglare Product Image" style="width: 438px; height: 238px">
                                                    <img class="secondary-img" src="{{url('/')}}/{{$product->image2}}" alt="Homeglare Product Image" style="width: 438px; height: 238px">
                                                </a>
                                                <div class="add-actions">
                                                    <ul>
                                                        <li><a class="hiraola-add_cart"  onclick="addToCart({{$product->id}})" data-toggle="tooltip" data-placement="top" title="Add To Cart"><i class="ion-bag"></i></a>
                                                        </li>
                                                        <li class="quick-view-btn" data-toggle="modal" data-target="#exampleModalCenter" data-myid="{{$product->id}}"><a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="Quick View"><i class="ion-eye"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="hiraola-product_content">
                                                <div class="product-desc_info">
                                                    <h6><a class="product-name" href="/product-detail/{{$product->slug}}">{{str_limit($product->name, 18)}}</a></h6>
                                                    <div class="price-box">
                                                        <span class="new-price">Rs. {{$product->sell_price}}</span>
                                                    </div>
                                                    <div class="additional-add_action">
                                                        <ul>
                                                            <li><a class="hiraola-add_compare"  onclick="addToWishlist({{$product->id}})" data-toggle="tooltip" data-placement="top" title="Add To Wishlist"><i
                                                                    class="ion-android-favorite-outline"></i></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="rating-box">
                                                        @if($product->Rating)
                                                          <span class="fa fa-star {{$product->Rating >= 1 ? 'star-checked' : ''}}"></span>
                                                          <span class="fa fa-star {{$product->Rating >= 2 ? 'star-checked' : ''}}"></span>
                                                          <span class="fa fa-star {{$product->Rating >= 3 ? 'star-checked' : ''}}"></span>
                                                          <span class="fa fa-star {{$product->Rating >= 4 ? 'star-checked' : ''}}"></span>
                                                          <span class="fa fa-star {{$product->Rating >= 5 ? 'star-checked' : ''}}"></span>
                                                        @else
                                                        <span class="invisible">Unrated</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                    @endif
                                    <!-- Homeglare Slide Item Area End Here -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Homeglare Product Tab Area Three End Here -->
        <!-- testimonial sectio -->
        @include('homeglare.testimonial')

@endsection

@section('js-script')
<script>
    // Add to Cart
    function addToCart(id){
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        // alert(id);
        var quantity = 1;

      $.ajax({
         url: '/add-to-cart',
         type: 'POST',
         data: {_token: CSRF_TOKEN, id: id, quantity: quantity},
         success: function (data) {
           // alert(data.cartItem);
           $('#cartItem').html(data.cartItem)
         },
         failure: function (data) {
           alert(data.message);
         }
      });
   }

   // Add to Wishlist
   function addToWishlist(id){
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      userId = <?php if(!empty(Auth::user()->id)){echo Auth::user()->id;} else {echo 0;}?> ;

      if(userId == 0){
        alert('Please Login First');
      }
      else{
        // alert(id);

      $.ajax({
         url: '/add-to-wishlist',
         type: 'POST',
         data: {_token: CSRF_TOKEN, id: id, userId : userId},
         success: function (data) {
           // alert(data.wishItem);
           $('#wishItem').html(data.wishItem);
         },
         failure: function (data) {
           alert(data);
         }
      });
      }
   }
</script>

@endsection
