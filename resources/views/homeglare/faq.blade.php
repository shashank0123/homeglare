@extends('layouts/ecommerce')

@section('content')


        <!-- Begin Hiraola's Breadcrumb Area -->
        <!-- <div class="breadcrumb-area">
            <div class="container">
                <div class="breadcrumb-content">
                    <h2>Other</h2>
                    <ul>
                        <li><a href="/">Home</a></li>
                        <li class="active">FAQ</li>
                    </ul>
                </div>
            </div>
        </div> -->
        <!-- Hiraola's Breadcrumb Area End Here -->
        <!-- Begin Hiraola's Frequently Area -->
        <div class="frequently-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="frequently-content">
                            <div class="frequently-desc">
                                <h3>Below are frequently asked questions, you may find the answer for yourself</h3>
                                <p></p>
                            </div>
                        </div>
                        
                        <!-- Begin Frequently Accordin -->
                        <div class="frequently-accordion">
                            <div id="accordion">
                                @if($faq)
                                  @foreach($faq as $index => $faqData)
                                  <div class="card {{$index == 0 ? 'actives': ''}}">
                                      <div class="card-header" id="heading{{$index}}">
                                          <h5 class="mb-0">
                                              <a href="javascript:void(0)" class="{{$index > 0 ? 'collapsed': ''}}" data-toggle="collapse" data-target="#collapse{{$index}}" aria-expanded="true" aria-controls="collapse{{$index}}">
                                                  {{$faqData->question}}
                                              </a>
                                          </h5>
                                      </div>
                                      <div id="collapse{{$index}}" class="collapse {{$index == 0 ? 'show': ''}}" aria-labelledby="heading{{$index}}" data-parent="#accordion">
                                          <div class="card-body">
                                            {{$faqData->answer}}
                                          </div>
                                      </div>
                                  </div>
                                  @endforeach
                                @endif
                            </div>
                        </div>
                        <!--Frequently Accordin End Here -->
                    </div>
                </div>
            </div>
        </div>
        <!-- Hiraola's Frequently Area End Here -->

@endsection
