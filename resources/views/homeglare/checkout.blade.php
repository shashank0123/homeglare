
@extends('layouts/ecommerce')

@section('content')
        <!-- Begin Hiraola's Breadcrumb Area -->
        <!-- <div class="breadcrumb-area">
            <div class="container">
                <div class="breadcrumb-content">
                    <h2>Other</h2>
                    <ul>
                        <li><a href="/">Home</a></li>
                        <li class="active">Checkout</li>
                    </ul>
                </div>
            </div>
        </div> -->
        <!-- Hiraola's Breadcrumb Area End Here -->
        <!-- Begin Hiraola's Checkout Area -->
        @if(session('error'))
            <div class="alert alert-success">
                {{ session('error') }}
            </div>
        @endif
      <form action="{{route('order-product')}}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="checkout-area">
            <div class="container">

                @if(Auth::user())
                <!-- <div class="row">
                    <div class="col-12">
                        <div class="coupon-accordion">
                            <h3>Have a coupon? <span id="showcoupon">Click here to enter your code</span></h3>
                            <div id="checkout_coupon" class="coupon-checkout-content">
                                <div class="coupon-info">
                                    <form action="javascript:void(0)">
                                        <p class="checkout-coupon">
                                            <input class="form-control @error('country') is-invalid @enderror" value="{{ old('country') }}" placeholder="Coupon code" type="text">
                                            <input class="form-control @error('country') is-invalid @enderror" value="{{ old('country') }}" class="coupon-inner_btn" value="Apply Coupon" type="submit">
                                        </p>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->
                @endif
                <div class="row">
                    <div class="col-lg-6 col-12">
                      @if($allAddress)
                      <div class="row">
                          <div class="col-12">
                              <div class="coupon-accordion">
                                  <h3><span id="showcoupon">Saved address</span></h3>
                                  <div id="checkout_coupon" class="coupon-checkout-content">
                                    @foreach($allAddress as  $key => $addr)
                                      <div class="coupon-info">
                                        <div class="custom-control custom-radio">
                                          <input type="radio" id="address_{{ $loop->iteration }}" name="customRadio" class="custom-control-input select-address">
                                          <label class="custom-control-label" for="address_{{ $loop->iteration }}">
                                            <address>
                                                  {{$addr->address}} {{$addr->city}} {{$addr->state}} {{$addr->country}} {{$addr->pin_code}}
                                            </address>
                                          </label>
                                        </div>

                                      </div>
                                      @endforeach
                                  </div>
                              </div>
                          </div>
                      </div>
                      @endif
                            <div class="checkbox-form">
                                
                                <input type="hidden" name="add_id" value="@if(!empty($address)){{$address->id}}@endif">
                                
                                <h3>Billing Details</h3>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="country-select clearfix">
                                            <label>Country <span class="text-danger">*</span></label>
                                            <!-- <select class="myniceselect nice-select wide @error('country') is-invalid @enderror" value="india" disabled name="country">
                                              <option data-display="India" value="india">India</option>
                                            </select> -->
                                            <input class="form-control @error('country') is-invalid @enderror" value="india" disabled name="country">
                                        </div>
                                        @error('country')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="col-md-12">
                                        <div class="checkout-form-list">
                                            <label>Address <span class="text-danger">*</span></label>
                                            <input class="form-control @error('address') is-invalid @enderror" value="{{ $address ? $address->address: old('address') }}" placeholder="Complete address" type="text"name="address" required>
                                        </div>
                                        @error('address')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="col-md-12">
                                        <div class="checkout-form-list">
                                            <label>Town / City <span class="text-danger">*</span></label>
                                            <input class="form-control @error('city') is-invalid @enderror" value="{{ $address ? $address->city: old('city') }}" id="city" type="text" name="city" pattern="^[a-zA-Z\s]*$"  onkeyup="checkInputValue(this)" required>
                                        </div>
                                        @error('city')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="col-md-6">
                                        <div class="checkout-form-list">
                                            <label>State <span class="text-danger">*</span></label>
                                            <input class="form-control @error('state') is-invalid @enderror" value="{{$address ? $address->state: old('state') }}" onkeyup="checkInputValue(this)" pattern="^[a-zA-Z\s]*$"  type="text" id="state" name="state" required>
                                        </div>
                                        @error('state')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="col-md-6">
                                        <div class="checkout-form-list">
                                            <label>Postcode / Zip <span class="text-danger">*</span></label>
                                            <input class="form-control @error('pin_code') is-invalid @enderror" value="{{$address ? $address->pin_code: old('pin_code') }}" type="text" pattern="^\d+$" name="pin_code" id="pin_code" onkeyup="checkInputValue(this)" maxlength="6" required />
                                        </div>
                                        @error('pin_code')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="col-md-6">
                                        <div class="checkout-form-list">
                                            <label>Phone <span class="text-danger">*</span></label>
                                            <input class="form-control @error('phone') is-invalid @enderror" value="{{$address ? $address->phone: old('phone') }}" type="text" pattern="^\d+$" name="phone" id="phone" onkeyup="checkInputValue(this)" maxlength="10" id="phone" />
                                        </div>
                                        @error('phone')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="different-address">
                                    <div class="ship-different-title">
                                        <h3 >
                                            <label>Ship to a different address?</label>
                                            <input class="form-control @error('ship-box') is-invalid @enderror" value="different" id="ship-box" name="ship_to_different_address" type="checkbox">
                                        </h3>
                                    </div>


                                    <div class="row" id="ship-box-info">

                                        <div class="">
                                        @if(!empty($allAddress))
                                        @foreach($allAddress as $address)
                                        <input type="radio" name="shipped" value="{{$address->id}}">
                                        {{$address->address}}, {{$address->city}}, {{$address->state}}, {{$address->country}} - {{$address->pin_code}}<br>
                                        Contact - {{$address->phone}}<br><br>   
                                        @endforeach
                                        @endif
                                    </div>
                                    <br><br>

                                    <h3>or New Address</h3>

                                        <div class="col-md-12">
                                            <div class="country-select clearfix">
                                                <label>Country <span class="text-danger">*</span></label>
                                                <!-- <select class="myniceselect nice-select wide @error('shipping_country') is-invalid @enderror" value="india" disabled name="shipping_country">
                                                  <option data-display="India" value="india">India</option>
                                                </select> -->
                                                <input class="form-control @error('shipping_country') is-invalid @enderror" value="india" disabled name="shipping_country">
                                            </div>
                                            @error('shipping_country')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-12">
                                            <div class="checkout-form-list">
                                                <label>Address <span class="text-danger">*</span></label>
                                                <input class="form-control @error('shipping_address') is-invalid @enderror"  placeholder="Complete address" type="text"name="shipping_address" >
                                            </div>
                                            @error('shipping_address')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-12">
                                            <div class="checkout-form-list">
                                                <label>Town / City <span class="text-danger">*</span></label>
                                                <input class="form-control @error('shipping_city') is-invalid @enderror"  id="shipping_city" type="text" name="shipping_city" pattern="^[a-zA-Z\s]*$"  onkeyup="checkInputValue(this)" />
                                            </div>
                                            @error('shipping_city')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-6">
                                            <div class="checkout-form-list">
                                                <label>State <span class="text-danger">*</span></label>
                                                <input class="form-control @error('shipping_state') is-invalid @enderror"  onkeyup="checkInputValue(this)" pattern="^[a-zA-Z\s]*$"  type="text" id="shipping_state" name="shipping_state" />
                                            </div>
                                            @error('state')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-6">
                                            <div class="checkout-form-list">
                                                <label>Postcode / Zip <span class="text-danger">*</span></label>
                                                <input class="form-control @error('shipping_pin_code') is-invalid @enderror"  type="text" pattern="^\d+$" name="shipping_pin_code" id="shipping_pin_code" onkeyup="checkInputValue(this)" maxlength="6" />
                                            </div>
                                            @error('shipping_pin_code')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>shipping_
                                            @enderror
                                        </div>
                                        <div class="col-md-6">
                                            <div class="checkout-form-list">
                                                <label>Phone <span class="text-danger">*</span></label>
                                                <input class="form-control @error('shipping_phone') is-invalid @enderror"  type="text" pattern="^\d+$" name="shipping_phone" id="shipping_phone" onkeyup="checkInputValue(this)" maxlength="10" />
                                            </div>
                                            @error('shipping_phone')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <!-- <div class="order-notes">
                                        <div class="checkout-form-list checkout-form-list-2">
                                            <label>Order Notes</label>
                                            <textarea id="checkout-mess" cols="30" rows="10" placeholder="Notes about your order, e.g. special notes for delivery."></textarea>
                                        </div>
                                    </div> -->
                                </div>
                            </div>
                    </div>
                    <div class="col-lg-6 col-12">
                        <div class="your-order">
                            <h3>Your order</h3>
                            <div class="your-order-table table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th class="cart-product-name">Product</th>
                                            <th class="cart-product-total">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      @if(session()->get('cart') != null)
                                        @foreach($productDetails as $product)
                                          <tr class="cart_item">
                                              <td class="cart-product-name"> {{$product->name}}<strong class="product-quantity">
                                              × {{$product->quantity}}</strong></td>
                                              <td class="cart-product-total"><span class="amount"><i class="fa fa-inr"></i> {{$product->total_price}}</span></td>
                                          </tr>
                                          @endforeach
                                        @endif
                                        <!-- <tr class="cart_item">
                                            <td class="cart-product-name"> Vestibulum suscipit<strong class="product-quantity">
                                            × 1</strong></td>
                                            <td class="cart-product-total"><span class="amount">£165.00</span></td>
                                        </tr> -->
                                    </tbody>
                                    <tfoot>
                                        <tr class="cart-subtotal">
                                            <th>Cart Subtotal</th>
                                            <td><span class="amount"><i class="fa fa-inr"></i> {{$totalAmount}}</span></td>
                                        </tr>
                                        <tr class="order-total">
                                          <input type="text" name="totalAmount" hidden value="{{$totalAmount}}">
                                            <th>Order Total</th>
                                            <td><strong><span class="amount"><i class="fa fa-inr"></i> {{$totalAmount}}</span></strong></td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <!-- <div class="payment-method">
                                <div class="payment-accordion">
                                    <div id="accordion">
                                        <div class="card">
                                            <div class="card-header" id="#payment-1">
                                                <h5 class="panel-title">
                                                    <a href="javascript:void(0)" class="" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                        Direct Bank Transfer.
                                                    </a>
                                                </h5>
                                            </div>
                                            <div id="collapseOne" class="collapse show" data-parent="#accordion">
                                                <div class="card-body">
                                                    <p>Make your payment directly into our bank account. Please use your Order
                                                        ID as the payment
                                                        reference. Your order won’t be shipped until the funds have cleared in
                                                        our account.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-header" id="#payment-2">
                                                <h5 class="panel-title">
                                                    <a href="javascript:void(0)" class="collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                        Cheque Payment
                                                    </a>
                                                </h5>
                                            </div>
                                            <div id="collapseTwo" class="collapse" data-parent="#accordion">
                                                <div class="card-body">
                                                    <p>Make your payment directly into our bank account. Please use your Order
                                                        ID as the payment
                                                        reference. Your order won’t be shipped until the funds have cleared in
                                                        our account.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-header" id="#payment-3">
                                                <h5 class="panel-title">
                                                    <a href="javascript:void(0)" class="collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                        PayPal
                                                    </a>
                                                </h5>
                                            </div>
                                            <div id="collapseThree" class="collapse" data-parent="#accordion">
                                                <div class="card-body">
                                                    <p>Make your payment directly into our bank account. Please use your Order
                                                        ID as the payment
                                                        reference. Your order won’t be shipped until the funds have cleared in
                                                        our account.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="order-button-payment">
                                        <input class="form-control @error('country') is-invalid @enderror" value="{{ old('country') }}" value="Place order" type="submit">
                                    </div>
                                </div>
                            </div> -->
                            <div class="row">
                              <div class="col-md-12">
                                <div class="custom-control custom-radio custom-control-inline">
                                  <input  type="radio" id="cash_on_delivery" name="payment_method" class="custom-control-input" value="cash_on_delivery" checked>
                                  <label class="custom-control-label" for="cash_on_delivery">Cash On Delivery</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                  <input  type="radio" id="pay_by_card" name="payment_method" value="pay_by_card" class="custom-control-input">
                                  <label class="custom-control-label" for="pay_by_card">Pay By Card</label>
                                </div>
                              </div>
                            </div>
                            <div class="order-button-payment">
                                <input value="Place order" class="btn-submit" type="submit">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </form>
        <!-- Hiraola's Checkout Area End Here -->
        <script type="text/javascript">

        /* Start input check regex */
          function checkInputValue(field) {
            var value = field.value;
            var fieldId = field.id;
            var pattern = field.pattern;
            var pattern = new RegExp(field.pattern);
            var re = pattern.test(value);
            // var re = value.match(pattern);
            if(!re) {
                $('#' + fieldId + '_error').removeClass('d-none');
                if (document.getElementById(fieldId + '_error')) {
                  $('#' + fieldId + '_error').removeClass('d-none');
                  $('.btn-submit').prop('disabled', true);
                } else {
                    $('#' + fieldId).after('<small id="' +fieldId+'_error" class="text-danger">This value is not valid</small>');
                    $('.btn-submit').prop('disabled', true);
                }
            } else {
                $('#' + fieldId + '_error').addClass('d-none');
                $('.btn-submit').prop('disabled', false)
            }
          };
        /* End input check regex*/
        </script>

@endsection
