

<div class="subscribe-area pb-100">
            <div class="container">
                <div class="subscribe-bg bg-img pt-45 pb-50 pl-20 pr-20" style="background-image:url(assets/images/bg/bg-3.jpg);">
                    <div class="row">
                        <div class="col-lg-6 ml-auto mr-auto">
                            <div class="subscribe-content-3 text-center">
                                <h2>Sign up &amp; Get all updates.</h2>
                                <div id="mc_embed_signup" class="subscribe-form-3 mt-20">
                                    <form id="mc-embedded-subscribe-form" class="validate subscribe-form-style" novalidate="" target="_blank" name="mc-embedded-subscribe-form" method="post" action="http://devitems.us11.list-manage.com/subscribe/post?u=6bbb9b6f5827bd842d9640c82&amp;id=05d85f18ef">
                                        <div id="mc_embed_signup_scroll" class="mc-form">
                                            <input class="email" type="email" required="" placeholder="Enter Your E-mail" name="EMAIL" value="">
                                            <div class="mc-news" aria-hidden="true">
                                                <input type="text" value="" tabindex="-1" name="b_6bbb9b6f5827bd842d9640c82_05d85f18ef">
                                            </div>
                                            <div class="clear">
                                                <input id="mc-embedded-subscribe" class="button" type="submit" name="subscribe" value="">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <footer class="footer-area">

<div class="feature-area ">
            <div class="container-fluid">
                <div class="feature-border feature-border-about">
                    <div class="row">
                        <div class="col-lg-3 col-md-6 col-sm-6">
                            <div class="feature-wrap mb-30 text-center">
                                <img src="assets/images/icon-img/feature-icon-1.png" alt="">
                                <h5>Best Product</h5>
                                <span>Best Queality Products</span>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6">
                            <div class="feature-wrap mb-30 text-center">
                                <img src="assets/images/icon-img/feature-icon-2.png" alt="">
                                <h5>100% fresh</h5>
                                <span>Best Queality Products</span>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6">
                            <div class="feature-wrap mb-30 text-center">
                                <img src="assets/images/icon-img/feature-icon-3.png" alt="">
                                <h5>Secure Payment</h5>
                                <span>Best Queality Products</span>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6">
                            <div class="feature-wrap mb-30 text-center">
                                <img src="assets/images/icon-img/feature-icon-4.png" alt="">
                                <h5>Winner Of 15 Awards</h5>
                                <span>Healthy food and drink 2019</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>





            <div class="footer-top pt-105 pb-75 default-overlay footer-overlay">
                <div class="container-fluid">
                    <div class="row">

<div class="col-lg-4 col-md-6 col-12 col-sm-6">
                            <div class="footer-widget footer-contact-wrap-2 mb-40">
                                <a href="#"><img src="assets/images/logo/logo.png" alt="logo"></a>
                                <div class="footer-contact-content-2">
                                    <p>The administration we offer you is everything to us. Our expert site makes picking your apparatus as simple as could be expected under the circumstances. Putting in your request online is sheltered with our 100% verified site, so you can feel loose and certain your subtleties are completely secured</p>
                                  
                                   
                                    <div class="footer-social-hm5">
                                        <span>Follow US</span>
                                        <ul>
                                            <li><a href="#"><i class=" ti-facebook"></i></a></li>
                                            <li><a class="twitter" href="#"><i class="ti-twitter-alt"></i></a></li>
                                            <li><a class="pinterest" href="#"><i class="ti-pinterest"></i></a></li>
                                            <li><a class="instagram" href="#"><i class="ti-instagram"></i></a></li>
                                            <li><a class="google" href="#"><i class="ti-google"></i></a></li>

                                           
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <div class="col-lg-2 col-md-6 col-12 col-sm-6">
                            <div class="footer-widget mb-30">
                                <div class="footer-title-2">
                                    <h3>Service</h3>
                                </div>
                                <div class="footer-list-2">
                                    <ul>
                                        <li><a href="cart.php">Cart</a></li>
                                        <li><a href="my-account.php">My Account</a></li>
                                        <li><a href="wishlist.php">Wishlist</a></li>
                                        <li><a href="checkout.php">Check-Out</a></li>
                                       
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-6 col-12 col-sm-6">
                            <div class="footer-widget mb-30">
                                <div class="footer-title-2">
                                    <h3>Information</h3>
                                </div>
                                <div class="footer-list-2">
                                    <ul>
                                        <li><a href="about-us.php">About Us</a></li>
                                        <li><a href="privacy.php">Privacy Policy</a></li>
                                        <li><a href="return.php">Return Policy</a></li>
                                        <li><a href="contact.php">Contact us</a></li>
                                        <li><a href="faq.php">FAQ</a></li>
                                      
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-12 col-sm-6">
                            <div class="footer-widget mb-30">
                                <div class="footer-title-2">
                                    <h3>Contact</h3>
                                </div>
                                <div class="footer-contact-wrap">
                                    <p>Subscribe to our newsletters now and stay up-to-date with new collections.</p>
                                    <div id="mc_embed_signup" class="subscribe-form-2 mt-20">
                                        <form id="mc-embedded-subscribe-form" class="validate subscribe-form-style" novalidate="" target="_blank" name="mc-embedded-subscribe-form" method="post" action="http://devitems.us11.list-manage.com/subscribe/post?u=6bbb9b6f5827bd842d9640c82&amp;id=05d85f18ef">
                                            <div id="mc_embed_signup_scroll" class="mc-form">
                                                <input class="email" type="email" required="" placeholder="Enter Your E-mail" name="EMAIL" value="">
                                                <div class="mc-news" aria-hidden="true">
                                                    <input type="text" value="" tabindex="-1" name="b_6bbb9b6f5827bd842d9640c82_05d85f18ef">
                                                </div>
                                                <div class="clear">
                                                    <input id="mc-embedded-subscribe" class="button" type="submit" name="subscribe" value="">
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="footer-contact-content">
                                        <p><i class=" ti-location-pin "></i>7C-191 sector -1 Bawana industrial area Delhi. </p>
                                        <p><i class=" ti-headphone-alt "></i>Call Us: +91 8826361066 +91 8802030809</p>

                                         <p><i class=" ti-email"></i>Email: support@homeglare.com. </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-bottom bg-black-2 ptb-20">
                <div class="container">
                    <div class="copyright copyright-2 text-center">
                        <p>Copyright © <a href="/">HomeGlare</a>. All Right Reserved</p>
                    </div>
                </div>
            </div>
        </footer>
        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-5 col-sm-6 col-xs-12">
                                <div class="tab-content quickview-big-img">
                                    <div id="pro-1" class="tab-pane fade show active">
                                        <img src="assets/images/product/quickview-l1.jpg" alt="">
                                    </div>
                                    <div id="pro-2" class="tab-pane fade">
                                        <img src="assets/images/product/quickview-l2.jpg" alt="">
                                    </div>
                                    <div id="pro-3" class="tab-pane fade">
                                        <img src="assets/images/product/quickview-l3.jpg" alt="">
                                    </div>
                                    <div id="pro-4" class="tab-pane fade">
                                        <img src="assets/images/product/quickview-l2.jpg" alt="">
                                    </div>
                                </div>
                                <!-- Thumbnail Large Image End -->
                                <!-- Thumbnail Image End -->
                                <div class="quickview-wrap mt-15">
                                    <div class="quickview-slide-active owl-carousel nav nav-style-2" role="tablist">
                                        <a class="active" data-toggle="tab" href="#pro-1"><img src="assets/images/product/quickview-s1.jpg" alt=""></a>
                                        <a data-toggle="tab" href="#pro-2"><img src="assets/images/product/quickview-s2.jpg" alt=""></a>
                                        <a data-toggle="tab" href="#pro-3"><img src="assets/images/product/quickview-s3.jpg" alt=""></a>
                                        <a data-toggle="tab" href="#pro-4"><img src="assets/images/product/quickview-s4.jpg" alt=""></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-7 col-sm-6 col-xs-12">
                                <div class="product-details-content quickview-content">
                                    <span>Life Style</span>
                                    <h2>LaaVista Depro, FX 829 v1</h2>
                                    <div class="product-ratting-review">
                                        <div class="product-ratting">
                                            <i class="la la-star"></i>
                                            <i class="la la-star"></i>
                                            <i class="la la-star"></i>
                                            <i class="la la-star"></i>
                                            <i class="la la-star-half-o"></i>
                                        </div>
                                        <div class="product-review">
                                            <span>40+ Reviews</span>
                                        </div>
                                    </div>
                                   <!--  <div class="pro-details-color-wrap">
                                        <span>Color:</span>
                                        <div class="pro-details-color-content">
                                            <ul>
                                                <li class="green"></li>
                                                <li class="yellow"></li>
                                                <li class="red"></li>
                                                <li class="blue"></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="pro-details-size">
                                        <span>Size:</span>
                                        <div class="pro-details-size-content">
                                            <ul>
                                                <li><a href="#">s</a></li>
                                                <li><a href="#">m</a></li>
                                                <li><a href="#">xl</a></li>
                                                <li><a href="#">xxl</a></li>
                                            </ul>
                                        </div>
                                    </div> -->
                                    <div class="pro-details-price-wrap">
                                        <div class="product-price">
                                            <span>₹210.00</span>
                                            <span class="old">₹230.00</span>
                                        </div>
                                        <div class="dec-rang"><span>- 30%</span></div>
                                    </div>
                                    <div class="pro-details-quality">
                                        <div class="cart-plus-minus">
                                            <input class="cart-plus-minus-box" type="text" name="qtybutton" value="02">
                                        </div>
                                    </div>
                                    <div class="pro-details-compare-wishlist">
                                       
                                        <div class="pro-details-wishlist">
                                            <a title="Add To Wishlist" href="#"><i class="la la-heart-o"></i> Add to wish list</a>
                                        </div>
                                    </div>
                                    <div class="pro-details-buy-now btn-hover btn-hover-radious">
                                        <a href="#">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal end -->
    </div>
    <!-- JS
============================================ -->
    

    <!-- Modernizer JS -->
    <script src="assets/js/vendor/modernizr-3.6.0.min.js"></script>
    <!-- Modernizer JS -->
    <script src="assets/js/vendor/jquery-3.3.1.min.js"></script>
    <!-- Popper JS -->
    <script src="assets/js/vendor/popper.js"></script>
    <!-- Bootstrap JS -->
    <script src="assets/js/vendor/bootstrap.min.js"></script>

    <!-- Slick Slider JS -->
    <script src="assets/js/plugins/countdown.js"></script>
    <script src="assets/js/plugins/counterup.js"></script>
    <script src="assets/js/plugins/images-loaded.js"></script>
    <script src="assets/js/plugins/isotope.js"></script>
    <script src="assets/js/plugins/instafeed.js"></script>
    <script src="assets/js/plugins/jquery-ui.js"></script>
    <script src="assets/js/plugins/jquery-ui-touch-punch.js"></script>
    <script src="assets/js/plugins/magnific-popup.js"></script>
    <script src="assets/js/plugins/owl-carousel.js"></script>
    <script src="assets/js/plugins/scrollup.js"></script>
    <script src="assets/js/plugins/waypoints.js"></script>
    <script src="assets/js/plugins/slick.js"></script>
    <script src="assets/js/plugins/wow.js"></script>
    <script src="assets/js/plugins/textillate.js"></script>
    <script src="assets/js/plugins/elevatezoom.js"></script>
    <script src="assets/js/plugins/sticky-sidebar.js"></script>
    <script src="assets/js/plugins/smoothscroll.js"></script>
     <!-- Main JS -->
    <script src="assets/js/main.js"></script>
   
</body>


</html>