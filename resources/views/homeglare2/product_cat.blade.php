<div class="tab-content jump" id="productCat">
   <div id="shop-1" class="tab-pane active">
      <div class="row">
         @if($searched_products !=null)
         <?php $count=0; ?>
         @foreach($searched_products as $product)
         <?php
         $count++;
         $discount = $product->mrp - $product->sell_price;

         if($discount>0)
            $discount = ($discount*100)/$product->mrp;
         ?>
         <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
            <div class="product-wrap product-border-1 product-img-zoom mb-15">
               <div class="product-img">
                  <a href="/product-detail/{{$product->slug}}"><img src="{{url('/'.$product->image1)}}" alt="product"></a>
                  <span class="price-dec">-{{round($discount,1)}}%</span>
                  <div class="product-action-2">
                     <a data-toggle="modal" data-target="#exampleModal" title="Quick View" data-myid="{{$product->id}}"><i class="la la-search"></i></a>
                     <a title="Add To Cart" onclick="addToCart({{$product->id}})"><i class="la la-cart-plus"></i></a>
                     <a title="Wishlist" onclick="addToWishlist({{$product->id}})"><i class="la la-heart-o"></i></a>
                  </div>
               </div>
               <div class="product-content product-content-padding">
                  <h4><a href="/product-detail/{{$product->slug}}">{{substr($product->name,0,18)}}.</a></h4>
                  <div class="price-addtocart">
                     <div class="product-price">
                        <span>₹{{$product->sell_price}}.00</span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         @endforeach
          @if($count==0)
                     <div style="text-align: center;margin-top: 50px"><h2>No product found</h2></div>
                     @endif
         @endif                        
      </div>
   </div>
   <div id="shop-2" class="tab-pane">

      @if($searched_products)
      <?php $count = 0; ?>
      @foreach($searched_products as $product)
      <?php $count++; ?>

      <div class="shop-list-wrap mb-30">
         <div class="row">
            <div class="col-xl-4 col-lg-5 col-md-6 col-sm-6">
               <div class="product-list-img">
                  <a href="/product-detail/{{$product->slug}}">
                     <img src="{{url('/'.$product->image1)}}" alt="Product Style">
                  </a>
                  <div class="product-list-quickview">
                     <a data-toggle="modal" data-target="#exampleModal" title="Quick View" data-myid="{{$product->id}}"><i class="la la-plus"></i></a>
                  </div>
               </div>
            </div>
            <div class="col-xl-8 col-lg-7 col-md-6 col-sm-6">
               <div class="shop-list-content">
                  {{-- <span>Chair</span> --}}
                  <h4><a href="/product-detail//{{$product->slug}}">{{substr($product->name,0,18)}}.</a></h4>
                  <div class="pro-list-price">
                     <span>₹{{$product->sell_price}}.00</span>
                     <span class="old-price">₹{{$product->mrp}}.00</span>
                  </div>
                  <p>@if($product->short_descriptions!=null){{strtoupper($product->short_descriptions)}}@endif</p>
                  <div class="product-list-action">
                     <a title="Wishlist" onclick="addToWishlist({{$product->id}})"><i class="la la-heart-o"></i></a>
                     <a title="Add To Cart" onclick="addToCart({{$product->id}})"><i class="la la-shopping-cart"></i></a>
                  </div>
               </div>
            </div>
         </div>
      </div>
      @endforeach

       @if($count==0)
                     <div style="text-align: center;margin-top: 50px"><h2>No product found</h2></div>
                     @endif
      @endif
   </div>   
</div>