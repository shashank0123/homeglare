@extends('layouts/homeglare')

@section('content')

<div class="about-us-area pt-90 pb-90">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                @if($companyInfo)
                <h2><span>Return Policy</span></h2><br>
                <p class="short_desc"><?php echo $companyInfo->return_policy; ?></p>
                @else
                <h2>Welcome To <span>{{env('APP_NAME')}}</span></h2>
                @endif
            </div>
        </div>
    </div>
</div>

@endsection
