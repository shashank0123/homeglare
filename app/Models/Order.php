<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Order extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'orders';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = ['user_id', 'address', 'price', 'payment_method', 'transaction_id', 'encrypted_payment_id', 'status'];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */
    public function getItemCountAttribute()
    {
        return \DB::table('order_items')->where('order_id', $this->id)
                                       ->count('item_id');
    }

    public function getItemDetailAttribute()
    {
        return \DB::table('order_items')
            ->leftjoin('products', 'products.id', '=', 'order_items.item_id')
            ->where('order_id', $this->id)
            ->select('products.name', 'order_items.quantity', 'order_items.price')
            ->get();
    }

    public function getPaymentMethodAttribute($value)
    {
      return $this->attributes['payment_method'] =  str_replace('_', ' ', $value);
    }

    public function getStatusAttribute($value)
    {
      return $this->attributes['status'] =  str_replace('_', ' ', $value);
    }
    // public function getOrderItemAttribute()
    // {
    //     return \DB::table('order_items')->where('order_id', $this->id)
    //                                    ->get();
    // }
    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
