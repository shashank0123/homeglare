<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Mail;
use Log;
use App\Models\Address;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'phone' => ['required','numeric'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        
        $user =  User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'access_type' => 'customer',
            'status' => 'active',
        ]);
        Address::create([
          'user_id' => $user->id,
          'address' => $data['address'],
          'phone' => $data['phone'],
        ]);

        $company = 'Homeglare';

       if($user){
          try {

            //To Admin
           Mail::send('mails.register-admin', ['request' => $data], function ($m) use ($company, $data) {
             $m->from( env('MAIL_USERNAME'), env('APP_NAME') );

             $m->to("support@homeglare.com", 'Admin')->subject('New User Registered to '.env('APP_NAME'));
           });

           //To User
           Mail::send('mails.register-user', ['request' => $data], function ($m) use ($company, $data) {
             $m->from( env('MAIL_USERNAME'), env('APP_NAME') );

             $m->to($data['email'],$data['name'])->subject('Welcome to '.env('APP_NAME'));
           });
         }
         catch(Exception $e)
         {

         }
       }

        return $user;
    }
}
