<?php
 
namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\Product;
use App\User;
use DB;
 
class PayController extends Controller
{
   
   // public function index()
   // {
   //      return view('homeglare.event');
   // }

   public function pay(Request $request){


     $api = new \Instamojo\Instamojo(
            config('services.instamojo.api_key'),
            config('services.instamojo.auth_token'),
            config('services.instamojo.url')
        );
 
    try {
        $response = $api->paymentRequestCreate(array(
            "purpose" => "Product Purchase",
            "amount" => $request->amount,
            "buyer_name" => "$request->name",
            "send_email" => true,
            "email" => "$request->email",
            "phone" => "$request->mobile_number",
            "redirect_url" => "http://homeglare.com/pay-success"
            ));

                     
            header('Location: ' . $response['longurl']);
            exit();
    }catch (Exception $e) {
        print('Error: ' . $e->getMessage());
    }
 }
 
 public function success(Request $request){

    $message = 'Something Went Wrong';

 	$data = $request->all();
 	$data = $data['payment_request_id'];

    return $data; 	
     try {
 
        $api = new \Instamojo\Instamojo(
            config('services.instamojo.api_key'),
            config('services.instamojo.auth_token'),
            config('services.instamojo.url')
        );
 
        $response = $api->paymentRequestStatus(request('payment_request_id'));
 		
        if( !isset($response['payments'][0]['status']) ) {
         return redirect()->back()->with('message',$message);
        } else if($response['payments'][0]['status'] != 'Credit') {
         return redirect()->back()->with('message',$message); 
        } 
        else if($response['payments'][0]['status'] == 'Credit'){
        	$email = $response['email'];

        	$user = User::where('email',$email)->first();
        			
        		$payment = Order::where('user_id',$user->id)->where('status','in_process')->orderBy('created_at','DESC')->first();

        	$txn_id = $response['payments'][0]['payment_id'];
        	
        	$payment->transaction_id = $txn_id;
        	$payment->encrypted_payment_id = $data;
        	$payment->status = 'order_booked';
        		$payment->update();
        		$message = 'Payment success';
                $orders = $payment;

        		session()->forget('cart');

if($orders){

try {
  $user = Auth::user();
  $productDetails = DB::table('order_items')
  ->join('products', 'products.id', '=', 'order_items.item_id')
  ->where('order_items.order_id', $orders->id)
  ->select('products.name', 'order_items.quantity', 'order_items.price')
  ->get();
                                  Log::info($productDetails);
                 Mail::send('mails.orderInvoice', ['user' => $user, 'order' => $orders, 'productDetails' => $productDetails],
                 function ($m) use ($user) {
                     $m->from( env('MAIL_USERNAME'), env('APP_NAME') );

                     $m->to($user->email, $user->name)->subject('Order Invoice');
                 });
}
catch (Exception $e){

}
}
    return view('homeglare2.ordersuccess')->with('message',$message);
        	
        }
      }catch (\Exception $e) {
         return redirect()->back()->with('message',$message);
     }

  }
}