<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;
use App\Models\Address;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Review;
use App\Wishlist;
use App\Models\Offer;
use App\Models\Banner;
use App\Models\Company;
use App\Models\Query;
use App\Models\Faq;
use App\Models\Newsletter;
use App\Models\Help;
use Log;
use DB;
use Auth;
use Mail;

class HomeglareController extends Controller
{
	public function getHome(){
		$companyInfo = Company::first();
		$maincategory = Category::where('status','Active')->where('category_id',0)->get();
		$slider_cat = Category::where('status','Active')->where('category_id',0)->orderBy('created_at','DESC')->limit(5)->get();
		$slider_banners = Banner::where('image_type','slider')->where('status','Active')->get();
		
		$medium_banners = Banner::where('image_type','medium')->where('status','Active')->get();

		$featured_products = Product::where('trending','yes')->orderBy('created_at','DESC')->limit(25)->get();
		$new_products = Product::orderBy('name','ASC')->orderBy('created_at','DESC')->limit(25)->get();
		$trending_products = Product::orderBy('created_at','DESC')->limit(3)->get();
		
		$offers = Offer::where('status','Active')->orderBy('created_at','DESC')->limit(3)->get();
		
		return view('homeglare2.home', compact('companyInfo','maincategory','slider_banners', 'medium_banners','featured_products','new_products','offers','slider_cat','trending_products'));
		// return view('homeglare2.home',compact('companyInfo'));
	}

	public function getAbout(){
		return view('homeglare2.about-us');
	}

	public function getCart(){
		return view('homeglare2.cart');
	}

	public function getCheckout(){
		$productDetails = array();
  $totalAmount = 0;
  if (session()->get('cart')) {
    foreach (session()->get('cart') as $key => $value) {
      $product = Product::where('id', $value->product_id)->first();
      $product['quantity'] = $value->quantity;
      $product['total_price'] = $product->sell_price * $value->quantity;
      $totalAmount += $product['total_price'];
      array_push($productDetails, $product);
    }
  } else {
    return redirect('/');
  }

  $success ='';
  $address = Address::where('user_id', Auth::user()->id)->first();
  if(isset($address))
  $allAddress = Address::where('user_id', Auth::user()->id)->where('id','!=',$address->id)->get();
else
	$allAddress="";

      // Log::info($address);
  return view('homeglare2.checkout', compact('productDetails', 'totalAmount', 'success', 'address', 'allAddress'));
		
	}

	public function getContact(){
		 $companyInfo = Company::first();
		return view('homeglare2.contact',compact('companyInfo'));
	}

	public function getFaq(){
		return view('homeglare2.faq');
	}

	public function getLogin(){
		return view('homeglare2.login');
	}

	public function getAccount(){
		$user = Auth::user();
  $addresses = "";
  $orderDetails = Order::where('user_id', $user->id)->where('status','order_booked')->get();
  $address = Address::where('user_id', $user->id)->first();
   $upd_add = Address::where('user_id', $user->id)->first();
  
  if(!empty($address))
  $addresses = Address::where('user_id', $user->id)->where('id','!=',$address->id)->get();
  $help = Help::where('user_id', $user->id)->get();
  // return view('homeglare.my-account', compact('user', 'orderDetails', 'address', 'help','addresses'));
		return view('homeglare2.my-account', compact('user', 'orderDetails', 'address', 'help','addresses','upd_add'));
	}

	public function getProductDetails(Request $request,$slug){
		$product = Product::where('slug',$slug)->first();
		$category = Category::where('id',$product->category_id)->first();
		$related_products = Product::where('category_id',$product->category_id)->get();
		$productReview = Review::where('product_id', $product->id)->avg('rating');
$countReview = Review::where('product_id', $product->id)->count();

$productReview = floor($productReview);
$reviews = Review::where('product_id',$product->id)->get();
		return view('homeglare2.product-details',compact('product','category','related_products','productReview','countReview','reviews'));
	}

	public function getProducts($id,Request $request){
		$checkId = Category::where('slug',$id)->first();
		$subcat = $checkId->id;

		$keyword = $request->product_keyword;
		$max_price = Product::where('status','Active')->max("sell_price");

		$cartproducts = array();
		$color = array();
		$brand = array();
		$material = array();
		$size = array();
		$quantity = array();
		$cnt = 0;
		if(session()->get('cart') != null){
			foreach(session()->get('cart') as $cart){
				$cartproducts[$cnt] = Product::where('id',$cart->product_id)->first();
				$quantity[$cnt] = $cart->quantity;
				$cnt++;
			}
		}
		else{
			$cartproducts = null;
			$quantity = 0;
		}

		$lastproduct = Product::where('status','Active')->orderBy('created_at','desc')->first();

		$maincategory = Category::where('category_id',0)->where('status','Active')->get();

		foreach($maincategory as $row){
			$count=0;
			$count=Product::where('category_id',$row->id)->where('status','Active')->count();
			$category = Category::where('category_id',$row->id)->where('status','Active')->get();
			if($category){
				foreach($category as $col){
					$count+=Product::where('category_id',$col->id)->where('status','Active')->count();
					$subcategory = Category::where('category_id',$col->id)->where('status','Active')->get();
					if($subcategory){
						foreach($subcategory as $set){
							$count+=Product::where('category_id',$set->id)->where('status','Active')->count();
						}
					}
				}
			}
		}

		$searched_products = array();
		$cat_ids = array();

		$total = array();
		$i=0;
		$sec = Category::where('slug',$id)->where('status','Active')->first();
		$cat_ids[0] = Category::where('id',$sec->id)->where('status','Active')->first();

		$cid = 1;
		$cnt_pro = 0;
		$count_catpro = 0;
		$idss = Category::where('category_id',$sec->id)->where('status','Active')->get();
		if($idss != null){
			foreach($idss as $ids){
				$count_catpro = 0;
				$cat_ids[$cid++] = $ids;
				$idsss = Category::where('category_id',$ids->id)->where('status','Active')->get();
				$count_catpro+=Product::where('category_id',$ids->id)->where('status','Active')->count();
				if($idsss != null){
					foreach($idsss as $ds){
						$cat_ids[$cid++] = $ds;
						$count_catpro+=Product::where('category_id',$ds->id)->where('status','Active')->count();
					}
					if($count==0)
						$total[$i++] = 0;
					else
						$total[$i++] = $count_catpro;
				}
			}
		}

		for($j=0 ; $j<$cid ; $j++){
			$products = Product::where('category_id',$cat_ids[$j]->id)->where('status','Active')->get();
			if($products != null){
				foreach($products as $pro){
					$searched_products[$cnt_pro] = $pro;
					$color[$cnt_pro] = $pro->product_color;
					$brand[$cnt_pro] = $pro->product_brand;
					$material[$cnt_pro] = $pro->product_material;
					$cnt_pro++;
				}
			}
		}

		$color = array_filter($color);
		$color = array_unique($color);

		$brand = array_filter($brand);
		$brand = array_unique($brand);

		$material = array_filter($material);
		$material = array_unique($material);

		shuffle($searched_products);

		return view('homeglare2.product',compact('maincategory','searched_products','cat_ids','total','lastproduct','max_price','cartproducts','quantity','id','color','brand','material'));
	}

	public function getRegister(){
		return view('homeglare2.register');
	}


	public function getWishlist(Request $request, $id){
		$wishlist = Wishlist::where('user_id',$id)->get();

  return view('homeglare2.wishlist',compact('wishlist'));
		
	}
	
	public function getSeller(){
		$offers = Offer::where('status','Active')->orderBy('created_at','DESC')->get();
		return view('homeglare2.seller',compact('offers'));
	}

	public function getOfferDetail(Request $request){
		$id = $request->id;
		$offer = Offer::find($id);
		return response()->json(['offer'=>$offer]);
	}

}
