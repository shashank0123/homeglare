<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ReviewRequest as StoreRequest;
use App\Http\Requests\ReviewRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;

/**
 * Class ReviewCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class ReviewCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Review');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/review');
        $this->crud->setEntityNameStrings('review', 'reviews');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        // $this->crud->setFromDb();
        $products = array();
        $productList = \App\Models\Product::get();
        foreach ($productList as $key => $value) {
            $products[$value->id] = $value->name;
        }
        $this->crud->addFields([

            ['name' => 'product_id', 'label' => 'Product Id', 'type' => 'select_from_array','options' => $products ],
            ['name' => 'name', 'label' => 'Name', 'type' => 'text'],
            ['name' => 'email', 'label' => 'Email', 'type' => 'text'],
            ['name' => 'review', 'label' => 'Review', 'type' => 'text'],
            ['name' => 'rating', 'label' => 'Rating ( Out of 5 )', 'type' => 'number'],
            ['name' => 'status', 'label' => "Status", 'type' => 'select_from_array', 'options' => ['Active' => 'Active', 'Deactive' => 'Deactive']],

        ]);
        // show column in table
        $this->crud->addColumn([
         'name' => 'name', // The db column name
         'label' => "Name", // Table column heading
         'type' => 'text'
         ]);
        $this->crud->addColumn([
                 'name' => 'email', // The db column name
                 'label' => "Email", // Table column heading
                 'type' => 'text'
                 ]);
       $this->crud->addColumn([
        'name' => 'rating', // The db column name
        'label' => "Rating", // Table column heading
        'type' => 'text'
        ]);
       $this->crud->addColumn([
                'name' => 'review', // The db column name
                'label' => "Review", // Table column heading
                'type' => 'text'
                ]);

       $this->crud->addColumn([
                'name' => 'status', // The db column name
                'label' => "Status", // Table column heading
                'type' => 'text'
              ]);

        $this->crud->addColumn([
                 'name' => 'product_id', // The db column name
                 'label' => "Product ID", // Table column heading
                 'type' => 'text'
                 ]);

        // add asterisk for fields that are required in ReviewRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
