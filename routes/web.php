<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', 'HomeglareController@getHome');
Route::get('/home', 'HomeglareController@getHome');
Route::get('/about', 'HomeglareController@getAbout');
Route::get('/sell-on-homeglare', 'HomeglareController@getSeller');
Route::post('/offer-detail', 'HomeglareController@getOfferDetail');
Route::get('/cart/{id}', 'HomeglareController@getCart');
Route::get('/checkout/{id}', 'HomeglareController@getCheckout');
Route::get('/contact', 'HomeglareController@getContact');
Route::get('/faq', 'HomeglareController@getFaq');
Route::get('/account/{id}', 'HomeglareController@getAccount');
Route::get('/product-detail/{slug}', 'HomeglareController@getProductDetails');
Route::get('/product/{slug}', 'HomeglareController@getProducts');
Route::get('/register', 'HomeglareController@getRegister');
Route::get('/wishlist/{id}', 'HomeglareController@getWishlist');


// Route::get('event', 'PayController@index');
 Route::post('pay', 'PayController@pay');
 Route::get('pay-success', 'PayController@success');


Route::get('/auth/redirect/facebook', 'SocialController@redirect');
Route::get('/callback/facebook', 'SocialController@callback');

Route::get('/redirect/google', 'SocialAuthGoogleController@redirect');
Route::get('/callback/google', 'SocialAuthGoogleController@callback');

Route::post('/add-to-cart','AjaxController@addToCart');
Route::post('/add-to-wishlist','AjaxController@addToWishlist');
Route::post('/delete-wishlist-item','AjaxController@deleteWishlistItem');
// Route::post('/newsletter','AjaxController@submitNewsletter');
Route::post('/newsletter', 'SiteController@createNewsletter')->name('newsletter');
Route::post('/cart/delete-product/{id}','AjaxController@deleteSessionData');
// Route::post('/cart/delete-product','AjaxController@deleteSessionData');
Route::post('/update-cart','AjaxController@updateCart');

Auth::routes();

Route::get('/home', 'HomeglareController@getHome');
Route::get('/', 'HomeglareController@getHome');
Route::post('/update-password', 'AjaxController@updatePassword');
Route::post('/update-profile', 'AjaxController@updateProfile');
// Route::get('/','SiteController@getIndex');
// Route::get('/home', 'SiteController@getIndex')->name('home');
// Route::get('/product-detail/{slug}','SiteController@getProductDetail');
// Route::get('/products/{slug}','SiteController@getCategoryProducts');

// Route::get('/searched-producs/{id}/{keyword}','SiteController@getSearchedProducts');
// Route::get('/cart/{id}','SiteController@getCart');
// Route::get('/checkout/{id}','SiteController@getCheckoutPage');
Route::post('/order-product', 'SiteController@orderProduct')->name('order-product');
Route::get('/order-booked', function()
{
  return view('homeglare2.ordersuccess');
});
// Route::get('/wishlist/{id}','SiteController@getWishlist');
Route::get('/about-us','SiteController@getAboutUs');
Route::get('/contact','SiteController@getContactUs');
Route::post('/contact-us', 'SiteController@contactUs')->name('contact-us');
Route::get('/faq','SiteController@getFaq')->name('faq');
Route::get('/terms&conditions','SiteController@getTermsandCondition');
Route::get('/privacy-policy','SiteController@getPrivacyPolicy');
Route::get('/return-policy','SiteController@getReturnPolicy');
Route::get('/my-account/{id}','SiteController@getMyAccount');
Route::get('/ordered-item-details/{order_id}', 'SiteController@orderedItemDetails')->name('ordered-item-details');
Route::get('/ordered-item-track/{order_id}', 'SiteController@orderedItemTrack')->name('ordered-item-track');
Route::resource('reply', 'ReplyController');

Route::get('/show-minicart','AjaxController@showMiniCart');
Route::get('/search-product/productsCat','AjaxController@showProductCat');
Route::get('/search-product/sort', 'AjaxController@sorting');
Route::get('/search-product/price-filter', 'AjaxController@price');
Route::get('/search-product/colored', 'AjaxController@colored');
Route::get('/search-product/rating', 'AjaxController@rating');
Route::resource('help-support', 'HelpSupportController');

Route::get('/get-product_details-data/{productId}', 'AjaxController@getProductDetailsData');
Route::post('/give-product-rating', 'SiteController@giveProductRating')->name('give-product-rating');
Route::post('/global-search-product', 'SiteController@globalSearchProduct')->name('global-search-product');

// snapSocialShare
Route::get('/shareLinkOnSocialSite/{siteKey}/{slug_url}/{product_id}', 'SiteController@shareLinkOnSocialSite');
//fb snap Social Share callback
Route::get('SharedLink/callback/{siteKey}', 'SiteController@SharedLinkCallback');
